## program unit
A STEP 7 program unit is either a function, function block, organizational block, data block or a user-defined type.

## executable program unit
An executable STEP 7 program unit is a program unit that can be executed, i.e., a function, a function block or an organizational block.

## CTL
Computation Tree Logic, a branching-time temporal logic to express formal requirements.

## LTL
Linear Temporal Logic, a linear-time temporal logic to express formal requirements.

## PLC
Programmable Logic Controller.

## SCL
Structured Control Language, a high-level STEP 7 programming language.

## STL
Statement List, a low-level, assembly-like STEP 7 programming language.

## CFA
Control Flow Automaton

## AST
Abstract Syntax Tree

