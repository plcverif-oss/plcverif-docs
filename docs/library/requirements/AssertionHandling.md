## Assertion handling

Checking the possible violation of assertions at certain locations of the source code provides a general way to describe requirements to check. This is why the CFA metamodels natively support location assertion annotations ([`AssertionAnnotation`](../../reference/CfaBaseXcore.md)). However, most model checkers do not support the concept of assertions and thus the assertions need to be represented in the CFA structure and by appropriate temporal logic requirement.

PLCverif provides built-in generic support to (1) inline assertion annotations (express them by the CFA structure), (2) generate the temporal logic requirement corresponding to the non-violation of the inlined assertions, and (3) diagnosing the counterexample (i.e., to trace back the counterexample to the violation of one of the assertions).
These facilities are used by the `cern.plcverif.library.requirement.assertion` assertion requirement plug-in, but other plug-ins can reuse them too.

The overview of the assertion-based requirement representation facilities can be seen on the figure below:

```uml
@startuml

title Assertion inlining and verification infrastructure


package cern.plcverif.base.models.cfa.transformation.assertion {
    class AssertInliner {
        {static} + transform(network, AssertInliningStrategy strategy) : AssertInliningResult
    }
    
    abstract class AssertInliningStrategy <<abstract>> {
    }
    hide AssertInliningStrategy members
    AssertInliner ..> AssertInliningStrategy
    
    class AssertInliningResult {
        + getErrorField() : FieldRef
        + getAssertion(long id) : Optional<InlinedAssertion>
        + getInliningStrategy() : AssertInliningStrategy
    }
    AssertInliner ..> AssertInliningResult
    AssertInliningResult --> AssertInliningStrategy
    
    class BoolAssertInliningStrategy <<singleton>> {
    }
    AssertInliningStrategy <|-- BoolAssertInliningStrategy
    hide BoolAssertInliningStrategy members
    AssertInliningStrategy --> BoolAssertInliningStrategy : <u>BOOL_STRATEGY</u>
    
    class IntAssertInliningStrategy <<singleton>> {
    }
    AssertInliningStrategy <|-- IntAssertInliningStrategy
    hide IntAssertInliningStrategy members
    AssertInliningStrategy --> IntAssertInliningStrategy : <u>INT_STRATEGY</u>
    
   
}

package cern.plcverif.verif.utils.diagnoser {
    abstract class AssertDiagnoser <<abstract>> {
        + diagnoseResult(...)
    }
    AssertDiagnoser *--> AssertDiagnosisPrinter : printer >
    AssertDiagnoser o--> AssertInliningResult : assertionInliningResult >
    
    class BoolAssertDiagnoser {
    }
    AssertDiagnoser <|-- BoolAssertDiagnoser
    hide BoolAssertDiagnoser members
    
    class IntAssertDiagnoser {
    }
    AssertDiagnoser <|-- IntAssertDiagnoser
    hide IntAssertDiagnoser members
    
    abstract class AssertDiagnosisPrinter <<abstract>> {
        + {abstract} appendAssertionViolatedMessage(...)
        + {abstract} noDiagnosisAvailable() : BasicFormattedString
    }
    AssertDiagnosisPrinter o--> AssertInliningResult : assertionInliningResult >
}

cern.plcverif.base.models.cfa.transformation.assertion -[hidden]-> cern.plcverif.verif.utils.diagnoser

package "Assertion requirement plug-in" #DDDDDD {
    class BoolAssertDiagnosisPrinter {
    }
    AssertDiagnosisPrinter <|-- BoolAssertDiagnosisPrinter
    hide BoolAssertDiagnosisPrinter members
    
    class IntAssertDiagnosisPrinter {
    }
    AssertDiagnosisPrinter <|-- IntAssertDiagnosisPrinter
    hide IntAssertDiagnosisPrinter members
}

package "DivByZero requirement plug-in" #DDDDDD {
    class DivByZeroAssertDiagnosisPrinter {
    }
    AssertDiagnosisPrinter <|-- DivByZeroAssertDiagnosisPrinter
    hide DivByZeroAssertDiagnosisPrinter members
}

@enduml
```

- **Inlining assertions (`cern.plcverif.base.models.cfa.transformation.assertion` package).**
  The [`AssertInliner`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/assertion/AssertInliner.html) utility class' `transform()` method is responsible for inlining the assertion annotations in the given CFA declaration. It will create a new field (_error field_) whose value is altered once an assertion is violated. The necessary new transitions and variable assignments are created as well by the `transform()` method.
  - The type of the created error field and the meaning of its different values are determined by the chosen assertion inlining strategy ([`AssertInliningStrategy`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/assertion/AssertInliningStrategy.html)).
    - The Boolean assertion inlining strategy (`AssertInliningStrategy.BOOL_STRATEGY`) creates a Boolean error field. Iff its value is true at the end of cycle _n_, one of the assertions has been violated in cycle _n_ or in a preceding cycle. It is not possible to determine which assertion has been violated.
    - The integer assertion inlining strategy (`AssertInliningStrategy.INT_STRATEGY`) creates an integer error field. Iff its value is non-zero at the end of cycle _n_, one of the assertions has been violated in cycle _n_ or in a preceding cycle. Based on the non-zero value, it is possible to determine which assertion has been violated.
  - The `transform()` method returns an [`AssertInliningResult`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/assertion/AssertInliningResult.html) object. This contains a reference to the error field created, the inlining strategy applied and the assertion corresponding the given integer ID.
- **Temporal logic representation.** Based on the `AssertInliningResult` object, it is possible to create a temporal logic requirement expressing that none of the assertions has been violated. A utility method `cern.plcverif.verif.utils.requirement.AssertRequirementUtil.createTemporalRequirement(VerificationProblem verifProblem, VerificationResult verifResult, RequirementRepresentationStrategy reqRepresentation, AssertInliningResult inliningResult)` is available to create the temporal logic requirement, adapted to the chosen assertion inlining strategy and the requirement representation strategy requested by the verification backend.
- **Diagnosing the result of assertion-based verification jobs (`cern.plcverif.verif.utils.diagnoser` package).** Once the assertion-based verification job has been executed, the verification result (i.e., whether the "no assertion has been violated" requirement is satisfied or violated) is available. However, by checking the value of the error field in the counterexample, more information can be acquired. Depending on the representation strategy, it can be determined in which execution cycle did the violation occur, and which assertion has been violated. The `AssertDiagnoser.diagnoseResult()` method performs this analysis.
  - Use the concrete diagnoser classes corresponding to the selected assertion inlining strategy, i.e., the `BoolAssertDiagnoser` or `IntAssertDiagnoser` classes' instances.
  - These diagnosers can determine the cycle of the violation and which assertion has been violated (if available). However, depending on the requirement plug-in, the human-readable visualization may be different. If the assertions were not created by the user but by the requirement plug-in, the user may not even know that assertions were checked. This is why the raw analysis data provided by the `AssertDiagnoser` classes is represented in the report using the different implementations of `AssertDiagnosisPrinter` class. Each requirement plug-in is expected to implement its own diagnosis printers for each inlining strategy it uses. This way the user can get meaningful feedback about the result of the verification job execution.
  