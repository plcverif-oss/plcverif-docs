# Reductions

The _CFA reductions_ are responsible to simplify the instance or declaration CFAs used in a [CFA job](../../architecture/api/PlatformExtensions.md#job). Each CFA reduction implements the [`cern.plcverif.base.extensions.reduction` extension point](../../architecture/api/PlatformExtensions.md#reduction), essentially providing an implementation for the `IReduction` extension for the [CFA jobs](../../architecture/api/PlatformExtensions.md#job). 


Currently, the library shipped with PLCverif contains the following reductions:
- [Basic reductions](BasicReductions.md)