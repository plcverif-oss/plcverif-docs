## Summary reporters

The _summary reporters_ are responsible to represent the results of a collection of verification job executions, based on the generated summary mementos.
Each summary reporter implements the `cern.plcverif.verif.summary.extensions.summaryreporter` extension point, essentially providing an implementation for the `ISummaryReporter` extension for the [summary reporter job](../../architecture/verif/SummaryJob.md). 

### JUnit summary reporter (`junit-summary`) {#junit-summary}

The JUnit summary report generator (`cern.plcverif.library.summaryreporter.junit`, ID: `junit-summary`) is responsible to generate summary report based on _summary mementos_ for a set of verification job executions, following the XML JUnit test result format. This format is particularly useful for the Jenkins integration that supports this output format.

#### Output
The output of the summary reporter is saved as `summary.xml`.

{% hint style='example' %}
**Example output.**
 ```
<testsuite tests="2">
    <testcase classname="PlcPerf01-01" name="model_PlcPerf01-01_algo_M1" time="1.733">
        <system-out>
            Backend: NusmvBackend<br/>
            Backend algorithm: nuxmv-Classic-dynamic-df<br/>
            Requirement: The assertion 'assertion1' is not violated.<br/>
        </system-out>
    </testcase>
    <testcase classname="PlcPerf01-02" name="model_PlcPerf01-02_algo_M1" time="2.5">
        <failure type="Violated">
            Verification result for requirement 'The assertion 'assertion1' is not violated.' was VIOLATED.
        </failure>
        <system-out>
            Backend: NusmvBackend<br/>
            Backend algorithm: nuxmv-Classic-dynamic-df<br/>
            Requirement: The assertion 'assertion1' is not violated.<br/>
        </system-out>
    </testcase>
</testsuite>
  ```
{% endhint %}


#### Settings

The JUnit summary report generator plug-in can be configured using PLCverif settings (typically with the prefix `job.reporters.?.`) which will be represented by a `JunitSummaryReporterSettings` instance.

| Setting name (Type) | Description | Default value |
| ------------------- | ----------- | ------------- |
| `unknown_as_error` (`boolean`) | Treat 'Unknown' results as 'Error'. | `false` |



### HTML summary reporter (`html-summary`) {#html-summary}

The HTML summary report generator (implemented in the `cern.plcverif.library.reporter.html` plugin's `summary` package, ID: `html-summary`) is responsible to generate summary report based on _summary mementos_ for a set of verification job executions in human-readable HTML format.

#### Output
The output of the summary reporter is saved as `summary.html`.

#### Settings

The HTML summary report generator plug-in can be configured using PLCverif settings (typically with the prefix `job.reporters.?.`) which will be represented by a `HtmlSummaryReporterSettings` instance.

| Setting name (Type) | Description | Default value |
| ------------------- | ----------- | ------------- |

Currently, no settings are defined for this plug-in.
