### ESBMC Integration

#### About ESBMC

[ESBMC](http://www.esbmc.org/) is an open source, permissively licensed, context-bounded C model checker based on satisfiability modulo theories based on a fork of CBMC. ESBMC is a joint project with the Federal University of Amazonas, University of Bristol, University of Manchester, University of Stellenbosch, and University of Southampton. The main advantages compared to CBMC are implementations of more modern algorithms (k-induction vs bounded model checking and SMT vs SAT).


#### Overview and Architecture
TODO


#### Model generation for ESBMC
Currently, the same model types as for CBMC are supported. CFA instance can cause bugs in some cases. 

##### Basic principles of C representation
See CBMC

##### Instance-based C representation
TODO

##### Declaration-based C representation
TODO


#####  Known limitations
- TODO


#### Executing ESBMC

TODO

##### Settings

TODO

#### Parsing the output

TODO
