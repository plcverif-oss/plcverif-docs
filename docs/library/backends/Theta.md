### Theta Integration

#### About Theta

[Theta](https://github.com/FTSRG/theta) is an open-source, generic, modular and configurable model checking framework developed at the [Fault Tolerant Systems Research Group](http://inf.mit.bme.hu/en) of [Budapest University of Technology and Economics](http://www.bme.hu/?language=en). Theta aims to support the design and evaluation of abstraction refinement-based algorithms for the reachability analysis of various formalisms.
The main distinguishing characteristic of Theta is its architecture that allows the definition of input formalisms with higher level language front-ends, and the combination of various abstract domains, interpreters, and strategies for abstraction and refinement.

More information about Theta can be found on its [GitHub page](https://github.com/FTSRG/theta) and in a tool paper [1]. 

#### Overview and Architecture

The integration consists of the following three main steps, which are detailed in the next sections.

1. First, the verification problem (the CFA and the requirement) of PLCverif is _transformed_ to the input language of Theta.
1. Then, Theta is _executed_ with the verification problem (input) and some parametrization (configuration) of the algorithm.
1. Finally, the output of Theta is parsed and _mapped back_ to the internal model of PLCverif. If the requirement is violated, a counterexample is also given.

The integration code is located in the project `cern.plcverif.library.backend.theta`. The main package contains some common classes required by the framework, or used by all three steps. Each step is located in its own package as follows.

1. `model` contains the model transformation classes,
1. `executor` contains the class for executing Theta and
1. `parser` contains the output parsing class.


#### Transformation to the Input Format of Theta

Theta has a similar CFA format as PLCverif, making most parts of the transformation only a straightforward syntactical mapping. However, there are a few differences which are explained in more detail.

##### Input Format of Theta

One of the possible input formats supported by Theta is a _simple control flow automata (CFA)_ formalism. A CFA is a _directed graph_ with

- _variables_,
- _locations_, with dedicated initial, final and error locations,
- _edges_ between locations, labeled with _statements_ over the variables.

Possible statements are

- _assignments_ in the form of `<variable> := <expression>`,
- _non-deterministic assignments_ in the form of `havoc <variable>`,
- _assumptions_ in the form of `assume <expression>` and
- _NOPs_ (no statement on the edge).

Theta includes a simple _textual representation_ for CFAs, for which an example can be seen below. Note, that in general an edge can also contain multiple statements separated by new lines.

```
main process counter {
    var x : int

    init loc L0
    loc L1
    loc L2
    loc L3
    final loc END
    error loc ERR

    L0 -> L1 { x := 0 }
    L1 -> L2 { assume x < 5 }
    L1 -> L3 { assume not (x < 5) }
    L2 -> L1 { x := x + 1 }
    L3 -> END { assume x <= 5 }
    L3 -> ERR { assume not (x <= 5) }
}
```

Algorithms in Theta can check whether the distinguished _error location is reachable or not_. This also implies a restriction on the kind of requirements that Theta can check (see later).

##### Transformation

Transformation is performed by the class `ThetaCfaTransformer`, which also uses the helper class `ThetaExpressionTransformer` for transforming expressions. Due to some limitations in Theta (see later), the preferred model format is _instance enumerated_.

###### Variables

The transformation of variables is quite straightforward. Each variable in PLCverif has its corresponding variable in Theta. The mapping between them is maintained in the context (`ThetaContext` class) for traceability.

###### Locations
The transformation of locations is also done by mapping each PLCverif CFA location to a Theta CFA location while maintaining the mapping in the context. However, Theta requires some _dedicated locations_ that are treated differently:
- Theta does not support initial values for variables (i.e., each variable starts with a nondeterministic initial value). Therefore, the _initial location_ in the Theta CFA is an additional location (with name `theta_init`), which is connected to the location that corresponds to the original initial location in the PLCverif CFA. This edge is annotated with the initial value assignments.
- The _final location_ of the Theta CFA corresponds to the end location of the PLCverif CFA.
- An additional location (with name `theta_error`) is introduced as the _error location_. This will be used later when transforming the requirement.

###### Transitions
The transformation of transitions is done by mapping each PLCverif CFA transition to a Theta CFA edge. The mapping does not need to be maintained in this case. A preprocessing transformation ensures that only assignment transitions remain.

First, the guard of the transition is mapped to a Theta assume statement, then each assignment is mapped to a Theta assignment. Each transition in PLCverif must have a guard, even if it is constant `True`. Theta does not have this constraint, therefore constant `True` guards are simply omitted from the Theta CFA.

Mapping the expressions in guards and assignments is done by the class `ThetaExpressionTransformer`. Each expression has its corresponding expression in Theta, with some limitations (see later). Theta does not support nondeterministic expressions, but a simple workaround is to use a havoc statement when the right hand side of an assignment is a nondeterministic expression.

###### Requirement
Theta can check the reachability of the error location, which can be used to verify _safety properties_ (in the for of `AG(<invariant>)`). However, in PLCverif such properties only need to be checked in the distinguished _end of cycle location (EoC)_. Therefore, outgoing transitions from the EoC location have an extra assume statement that checks if the invariant holds. Furthermore, there is an extra edge going from the EoC location to the error location, annotated with an assume statement ensuring that the invariant does not hold.

The requirement format `AG(<invariant>)` can be ensured by using the _implicit_ requirement representation strategy of PLCverif. The distinguished EoC location is included in the verification problem.

##### Limitations

The limitations of Theta are listed in this section. In most cases, they cause the backend to fail with an appropriate and informative error message. There are some exceptions though (indicated in the list), which only report a warning but then continue.

- Currently Theta only supports Boolean and mathematical (unbounded) integers. Floats, arrays are for example not supported, causing an error.
- Integers in PLCverif can be singed/unsigned and are of fixed size. All of them will be mapped to mathematical integers in Theta with a warning message.
- Integer to integer conversions are omitted in Theta with a warning message.
- Bit-level operations (e.g., and, or, shift) are not supported, causing an error.
- Only safety requirements (`AG(<invariant>)`) are supported. Other requirements cause an error.
- Only a single CFA instance is supported, which is checked at the beginning of the transformation.

Theta does not support function calls, but this is not a limitation as PLCverif supports inlining as a preprocessing transformation.

#### Executing Theta

Theta is written in Java (requires JRE8) and a command line interface (CLI) is deployed into a runnable jar file. Theta uses [Z3](https://github.com/Z3Prover/z3) as an SMT solver [2], which consists of 5 dll files. Executing Theta is performed by the class `ThetaExecutor` by collecting the arguments from the settings (`ThetaSettings` class) and executing it as a process with a given timeout.

##### Settings

The settings of Theta can be categorized into two main groups: _parameters of the verification engine_ and _parameters of the algorithm_.

###### Parameters of the verification engine

- `timeout`: Time limit for the execution, in seconds.
- `binary_path`: Path of the binary (jar) file of Theta.
- `lib_path`: Path of the libraries (Z3 dlls) required by Theta.

###### Parameters of the algorithm

Theta has the following command line parameters. Many of them can be modified via PLCverif settings.
- `--domain`: Abstract domain. Possible values are `PRED_BOOL` for Boolean predicate abstraction [3] [9], `PRED_CART` for Cartesian predicate abstraction [9], `PRED_SPLIT` for Boolean predicate abstraction with splitting disjunctions and `EXPL` for explicit value abstraction [4].
- `--refinement`: Refinement strategy. Possible values are `BW_BIN_ITP` for backward binary interpolation, `FW_BIN_ITP` for forward binary interpolation [5], `SEQ_ITP` for sequence interpolation [6] and `UNSAT_CORE` for unsat cores [7]. The latter is only available in the `EXPL` domain.
- `--search`: Search strategy. Possible values are `BFS` for breadth-first search, `DFS` for depth-first search and `ERR` for error location distance-based search.
- `--precgranularity`: Granularity of the precision of abstraction. Possible values are `LOCAL` for tracking separate precisions in each location and `GLOBAL` for a single, global precision.
- `--predsplit`: Splitting method for new predicates obtained from interpolation. Only applicable in the `PRED_*` domains. Possible values are `WHOLE` to keep the expression as is, `ATOMS` to split into atoms and `CONJUNCTS` to split into conjuncts.
- `--encoding`: Encoding of the CFA. Possible values are `SBE` for single block encoding and `LBE` for large block encoding [8].
- `--maxenum`: Maximal successors to enumerate using the SMT solver. Possible values are positive integers and `0` for infinity. Only applicable in the `EXPL` domain.
- `--initprec`: Initial precision of the abstraction. Possible values are `EMPTY` and `ALLVARS` for tracking all variables from the beginning. The latter is only available in the `EXPL` domain.
- `--loglevel`: Detailedness of the logging. It should be `RESULT` in order to be able to parse the output.
- `--cex`: A flag required to generate counterexample.

The parameters `--loglevel` and `--cex` are constant, and therefore not included in the settings. Other parameters have an entry in the settings with the same name. A default configuration is provided in the file _resources/settings/default.settings_.

#### Parsing the Output of Theta

Theta writes its output to the console. The first line indicates the result: safe, unsafe, exception or invalid parameters. The latter two cases and any other output causes an error in the backend. Safe and unsafe is mapped to satisfied and violated in PLCverif respectively.

When the requirement is violated (unsafe result) a _counterexample_ is also written to the console in a LISP-style textual format. A counterexample in Theta is an alternating sequence of states and actions. A state is a pair of a location and an explicit state, i.e., a valuation over the variables. An action is a sequence of statements. A (partial) example can be seen below.

```
(Trace
  (CfaState theta_init
    (ExplState))
  (CfaAction
    (assign MX1_1 false)
    ...    
    (assign __assertion_error 0))
  (CfaState loop_start
    (ExplState (MX3_4 false)
               ...
               (MX3_1 false)))
  (CfaAction
    (havoc IX1_2)
    ...
    (assign MX1_1 true))

    ...
    
  (CfaState theta_error
    (ExplState (MX4_1 true)
               ...
               (MX3_1 true))))
```

Parsing is performed by the class `ThetaParser`. It iterates through the counterexample and parses only those valuations that correspond to the EoC location. Each such valuation is a step in the instance counterexample in PLCverif. This also means that the size of the resulting counterexample in PLCverif is _less or equal_ to the size of the counterexample produced by Theta.

#### References

[1] [T. Tóth et al. Theta: a Framework for Abstraction Refinement-Based Model Checking. FMCAD 2017.](http://dx.doi.org/10.23919/FMCAD.2017.8102257)

[2] [L. De Moura and N. Bjørner. Satisfiability modulo theories: Introduction and applications, CACM, 2011.](http://dx.doi.org/10.1145/1995376.1995394)

[3] [S. Graf and H. Saidi. Construction of abstract state graphs with PVS. CAV 2017](http://dx.doi.org/10.1007/3-540-63166-6_10) 

[4] [D. Beyer and S. Löwe. Explicit-state software model checking based on CEGAR and interpolation. FASE 2013.](http://dx.doi.org/10.1007/978-3-642-37057-1_11)

[5] [T. A. Henzinger et al. Abstractions from proofs. POPL 2004.](http://dx.doi.org/10.1145/964001.964021)

[6] [Y. Vizel and O. Grumberg. Interpolation-sequence based model checking. FMCAD 2009.](http://dx.doi.org/10.1109/FMCAD.2009.5351148)

[7] [M. Leucker et al. A new refinement strategy for CEGAR-based industrial model checking. HSVT 2015.](http://dx.doi.org/10.1007/978-3-319-26287-1_10)

[8] [D. Beyer et al. Software model checking via large-block encoding. FMCAD 2009.](http://dx.doi.org/10.1109/FMCAD.2009.5351147)

[9] [T. Ball et al. Boolean and Cartesian Abstraction for Model Checking C Programs. TACAS 2001.](http://dx.doi.org/10.1007/3-540-45319-9_19)