### NuSMV integration

#### About NuSMV and nuXmv

[NuSMV](http://nusmv.fbk.eu/) is a symbolic model checker developed by FBK-IRST, University of Genoa and University of Trento. It is distributed under LGPL v2.1 license, allowing free commercial usage of NuSMV. [nuXmv](https://nuxmv.fbk.eu/) is a newer symbolic model checker (incorporating all features of NuSMV). The nuXmv model checker is only available for non-commercial or academic purposes according to its [license](https://es-static.fbk.eu/tools/nuxmv/downloads/LICENSE.txt). The re-distribution of nuXmv is not permitted.

As the input format of NuSMV and nuXmv is the same, in the following the term 'NuSMV' will often be used referring to both tools.

#### Overview and Architecture

The integration consists of the following three main steps, which are detailed in the next sections.

1. First, the verification problem (the CFA and the requirement) of PLCverif is _transformed_ to the input language of NuSMV.
1. Then, an execution script is generated for NuSMV, then NuSMV is _executed_ with the verification problem (SMV model including the requirement) and the generated script.
1. Finally, the output of NuSMV is _parsed_ and _mapped back_ to the internal model of PLCverif. If the requirement is violated, a counterexample is also given.

The integration code is located in the project `cern.plcverif.library.backend.nusmv`. The main package contains some common classes required by the framework, or used by all three steps. Each step is located in its own package as follows.

1. `model` contains the model transformation classes,
1. `executor` contains the class for executing NuSMV (including the execution script generation) and
1. `parser` contains the output parsing class.


#### Model generation for NuSMV

The NuSMV backend expects CFA models which are:
- instantiated (i.e., CFA is a `CfaNetworkInstance`),
- enumerated (i.e., there are no arrays in the CFA),
- inlined (i.e., there are no call transitions in the CFA).

The NuSMV integration uses the `EXPLICIT_WITH_VARIABLE` strategy for **requirement representation**, i.e., an additional variable needs to be created to represent the end of PLC cycle, which is then used in the requirement.

##### Basic principles
- Each CFA _variable_ is represented as a NuSMV variable, using the corresponding NuSMV data type. (See below for special handling of floating-point numbers). Variables and literals of `IntType` are represented as `word` in NuSMV.
- Each CFA _location_ is represented as a value in the value set of variable `loc`. This variable determines the currently active location.
- The CFA _transitions_ are represented variable-by-variable, i.e., for each variable a next-state function is defined, based on the currently active location (`loc`) and the guards defined for the transitions leaving that location.
- The requirement is represented as an `LTLSPEC` or `CTLSPEC`, depending on its type. Depending on the settings, an `AG(<invariant>)` or `G(<invariant>)` requirement may be represented as `INVARSPEC <invariant>`.

##### Notable characteristics
Notable characteristics of the NuSMV model generation:
- It is ensured that no name collision occurs with NuSMV's keywords (listed in `NusmvUtils.KEYWORDS`).
- To represent non-deterministic non-Boolean variables, additional variables are generated (prefixed with `NusmvModelBuilder.RANDOM_VAR_PREFIX`) which have non-deterministic values and can be used to assign non-deterministic values to CFA variables.
- NuSMV does not handle floating-point numbers. To overcome this limitation, an experimental fixed-point translation can be found in the `RemoveFloatsFromCfa` class. After applying this, each floating point number will be represented as `round(FLOATING * precision)` integer. By default, the value of `precision` is 1000.

#####  Known limitations
- Type conversion (`TypeConversion` in CFA) is only supported between integer variables (`IntType`).
- `POWER` and `BITROTATE` operators (`BinaryArithmeticOperator`) are not supported for the moment.
- Mixed CTL and LTL expressions are not supported.


#### Executing NuSMV

NuSMV (or nuXmv) is executed through the command line. Some parameters are passed via the `<model_id>.smv.script` file, some others are via command line arguments.
The command line options and the contents of the `.script` file are defined in `NusmvExecutionDetails`. 
NuSMV is called using the following pattern: `<binary_path> [-dynamic] [-df] -source <script_file> <model_file>`.

The output of NuSMV is generated to an external file (`<model_id>.smv.cex`).

##### Settings

The following settings are available for the NuSMV backend (defined by `NusmvSettings`):

- `timeout`: Time limit for the execution of NuSMV, in seconds. (default: 5 sec)
- `binary_path`: Path of the NuSMV/nuXmv binary. Can be a path relative to the program directory of PLCverif. (default: `.\tools\nuxmv\nuxmv.exe` on Windows and `./tools/nuxmv/nuXmv` on Linux)

- `algorithm`: Selects the algorithm (family) to be used by NuSMV. Can be `Classic` or `Ic3`. (default: `Classic`)  
   * If `Classic` is selected, the verification is based on the commands `check_ctlspec`, `check_ltlspec` or `check_invar`.
   * If `Ic3` is selected, the verification is based on the commands `check_ltlspec_klive`, `check_invar_ic3`. Checking CTL expressions is not supported with the option `Ic3`.
- `dynamic`: Enables dynamic reordering of variables in the backend. If true, the `-dynamic` parameter is passed to NuSMV. (default: `true`)
- `df`: Disables the computation of the set of reachable states. If true, the `-df` parameter is passed to NuSMV. (default: `true`)
- `req_as_invar`: Enables the representation of the requirements as invariants, if possible. (default: `false`)


#### Parsing the output

NuSMV writes its output to an external file (`<model_id>.smv.cex`) with the given configuration. It will contain the result of the verification (satisfied or violated), and the counterexample/witness if available.
In case of the IC3 algorithms, the result is written to the standard output. Note that there are some syntactic differences between the outputs of the different algorithms.

The variables and values of the counterexample are mapped back to the CFA after parsing the NuSMV output.

{% hint style='example' %}
An example output is shown below:

```
-- specification is false
-- as demonstrated by the following execution sequence
Trace Description: CTL Counterexample 
Trace Type: Counterexample 
    -> State: 1.1 <-
    loc = loc1
    int16Var1 = 0sd16_123
    uint16Var2 = 0ud16_123
    boolVar3 = FALSE
    unknownVar = FALSE
    EoC = FALSE
```
{% endhint %}