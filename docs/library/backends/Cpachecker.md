### CPAchecker Integration

#### About CPAchecker

[CPAchecker](https://cpachecker.sosy-lab.org/) is a strongly configurable framework/tool for C model checking, supporting a large variety of methods. CPAchecker is licensed under the Apache 2.0 License. 