## AST transformations on STEP 7 program files

In order to simplify the AST to CFA translation, certain modifications are performed directly on the parsed code (AST), preceding the AST to CFA translation. These transformations are implemented in `cern.plcverif.plc.step7.transformation` (each of them implements the `IStep7AstTransformation` interface) and briefly described below.

### Extract nested calls (`ExtractNestedCalls`) 

This AST transformation removes the nested calls (calls where the parameters contain other calls) which cannot be handled by the CFA. To eliminate them, temporary variables are introduced, the nested function calls are executed first. Their return values will be saved to the introduced temporary variables, then these values will be used in outer call. Note that this transformation is valid only because there is no short-circuit parameter evaluation in STEP 7 languages.

{% hint style='example' %}
**Example:** the `j := MAX(in1 := 1, in2 := MAX(in1 := 2, in2 := 3));` will be transformed into the equivalent of the following code:
```
___nested_ret_val1 := MAX(in1 := 2, in2 := 3);
j := MAX(in1 := 1, in2 := ___nested_ret_val1);
```
{% endhint %}

**Expectation after.** After this transformation has been performed, it can be expected that there are no nested calls in the CFA, i.e., subroutine calls are always at the root of Expression subtrees.


### Return value to output parameter (`RetValToOutputParam`)

In some STEP 7 languages it is permitted to use both output parameters and return values as means of providing data from a called function to the caller. To simplify the CFA representation of the program, the `RetValToOutputParam` AST transformation removes the use of return values and represents them using a special output parameter `RET_VAL`.

{% hint style='example' %}
**Example:** the `j := MAX(in1 := 1, in2 := 2)` will be transformed into `MAX(in1 := 1, in2 := 2, RET_VAL := j)`.
{% endhint %}

The `RetValToOutputParam` transformation should be executed after the `ExtractNestedCalls` transformation.

**Expectation after.** After this transformation has been performed, it can be expected that subroutine calls are not used in variable assignment, instead the equivalent `RET_VAL` output parameter will be used.


### Removal of `ANY_NUM`s from functions (`SpecializeFcsWithAnyNumParam`)

In general, the STEP 7 languages do not support function overloading (i.e., having two functions with the same name but different parameters). However, some special functions mimic overloading by the means of the special data type `ANY_NUM`. For example, the built-in function `MAX` takes two inputs of type `ANY_NUM` and returns another `ANY_NUM`. The data type `ANY_NUM` can be any numeric data type (both integer and floating-point types are supported). Based on the given input parameters, the appropriate concrete type will be determined and it will be used instead of each `ANY_NUM`. Effectively, the `MAX` function called with `REAL` inputs will return a `REAL` value, but if it is called with `INT`s, the return value is also an `INT`.

To support this particularity without introducing generic support for function overloading, the `SpecializeFcsWithAnyNumParam` AST transformation will specialize each function with `ANY_NUM` which is in use. For this, it creates new, specialized functions (e.g., `MAX#INT` and `MAX#REAL` for the previous example) where `ANY_NUM`s are replaced by the concrete numeric types, and the original function declaration is removed from the AST.

**Expectation after.** After this transformation has been performed, it can be expected that the AST does not contain any function with `ANY_NUM` type in use.


### Replacement of named constants (`SubstituteNamedConstantRefs`)

To simplify the AST to CFA translation and mainly the type computation, this AST transformation will replace each named constant use with its concrete value. The transformation takes into account the special cases, such the `CASE` statements where instead of `DirectNamedRef`s the constants are referred as `NamedConstantRef`s.
After this transformation the AST should not contain any expression that refers to a named constant declaration, also the AST will not contain any constant declaration (`ConstDeclarationBlock`, representing `CONST .. END_CONST` blocks).  

This transformation should happen before the type computation.


**Expectation after.** After this transformation has been performed, it can be expected that there are no named constant references used in expressions, they are all replaced with their definition.
