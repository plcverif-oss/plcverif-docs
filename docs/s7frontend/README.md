# STEP 7 frontend

The STEP 7 frontend_ is responsible to parse PLC programs written in any of the STEP 7 programming languages. The parser can be used in a [CFA job](../../architecture/api/PlatformExtensions.md#job). The STEP 7 frontend implements the [`cern.plcverif.base.extensions.parser` extension point](../../architecture/api/PlatformExtensions.md#parser), essentially providing an implementation for the `IParser` extension for the [CFA jobs](../../architecture/api/PlatformExtensions.md#job). 


Currently the following STEP 7 languages are supported:
  - SCL (Structured Control Language) v5.3
  - STL (Statement List)


**PLCverif extension.** The main goal of the STEP 7 fronted plug-in (`cern.plcverif.plc.step7`) is to parse the PLC programs (i.e., to produce their AST -- Abstract Syntax Tree) and then represent them as control-flow automata. The PLCverif language frontend extension is implemented in the `cern.plcverif.plc.step7.extension` plug-in. The [`Step7ParserExtension`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserExtension.html) class is a factory for the `Step7Parser` class. The [`Step7Parser`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7Parser.html) class is able to create a parser for the files (set using the `setFiles()` method) when the [`parseCode(JobResult)`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7Parser.html#parseCode-cern.plcverif.base.interfaces.data.JobResult-) method is called. 

The returned [`Step7ParserResult`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserResult.html) (implementing [`IParserLazyResult`]({{ api_docs_core_url }}/cern/plcverif/base/interfaces/IParserLazyResult.html)) will load the given program files, produce the AST (see [`getAst()`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserResult.html#getAst--)) and can generate the CFA declaration too (see [`generateCfa`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserResult.html#generateCfa-cern.plcverif.base.interfaces.data.JobResult-)). In addition, after generating the CFA, it is able to parse and serialize given atoms from/to their original PLC representation (see [`parseAtom`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserResult.html#parseAtom-java.lang.String-) and [`serializeAtom`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserResult.html#serializeAtom-cern.plcverif.base.models.expr.AtomicExpression-)).

In addition the plug-in provides a graphical editor for STEP 7 programs (see [`cern.plcverif.plc.step7.ui`]((https://gitlab.cern.ch/plcverif/cern.plcverif.plc.step7/tree/master/cern.plcverif.plc.step7.ui)), mainly generated from the [STEP 7 Xtext grammar](../reference/Step7Grammar.md).


**More information.** In more detail, the following steps are performed by the STEP 7 PLCverif plug-in:
- Parsing STEP 7 programs (see [`cern.plcverif.plc.step7`](https://gitlab.cern.ch/plcverif/cern.plcverif.plc.step7/tree/master/cern.plcverif.plc.step7)) based on the [STEP 7 Xtext grammar](../reference/Step7Grammar.md) describing the SCL and STL programs' syntaxes.
- Convert parsed STEP 7 programs to CFA declaration (implemented by the [`cern.plcverif.plc.step7.cfa`](https://gitlab.cern.ch/plcverif/cern.plcverif.plc.step7/tree/master/cern.plcverif.plc.step7.cfa) plug-in, see in [this section](CfaRepresentation.md)).
  * Determine the scope of the translation, i.e., collect the blocks and variables that need to be represented in the CFA. This is implemented in the [`TranslationScope`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/cfa/impl/TranslationScope.html) class.
  * Perform [AST transformations](AstTransformations.md) to simplify the AST that is to be represented.
  * Determine the type of the expressions and sub-expressions in the PLC programs using the [`Step7TypeComputer`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/typecomputer/Step7TypeComputer.html) class
  * [Represent the AST as CFA declarations](CfaRepresentation.md).

