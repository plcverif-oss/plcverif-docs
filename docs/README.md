# Introduction

This document provides a detailed documentation for the PLCverif project.

- The **User Documentation** chapter introduces the PLCverif platform's usage from the user's point of view.
- The **Project Description** chapter describes the goals and the scope of the project.
- The **Architecture** chapter overviews the services provided by the PLCverif framework, the ways to extend it, and the common data structures.
- The **Library** chapter describes the various plug-ins shipped with PLCverif to perform various parts of the formal verification: requirement definition, model checking, reporting, etc.
- The **STEP 7 frontend** chapter is dedicated to the parser infrastructure handling the Siemens SCL and STL programming languages.
- The **Development** chapter discusses the development, compilation and releasing infrastructure.
- The **Reference** chapter provides detailed reference descriptions of the different metamodels and grammars.