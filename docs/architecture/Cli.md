# Command line interface (CLI)

PLCverif has a lightweight command line interface (implemented in the `cern.plcverif.cli` package). As the entry point of the PLCverif platform is a settings tree describing the job to be executed, the only task of the command line interface is to parse the settings provided as command line parameters.

Two formats are supported by the current implementation:
- `plcverif-cli -settings1=value1 -settings2.subkey2=value2 ...`, i.e., all settings are provided as command line parameters.
- `plcverif-cli settingsfile.txt -settings1=value1 -settings2.subkey2=value2 ...`, i.e., the settings are provided in an external file (separated by new line characters), and optionally some more settings are provided as command line parameters.

Read more about the textual representation of settings in [the corresponding documentation](api/Settings.md#textual_syntax).


![](verif/img/fullSequence_sd.png)

