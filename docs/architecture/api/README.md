# Platform description

This section describes the basic platform services provided by PLCverif.

- The [Platform extensions](PlatformExtensions.md) subsection explains the workflow model and the platform-level support for language frontend handling and CFA reductions.
- The [Settings](Settings.md) subsection is dedicated to the unified serialization and deserialization of job configuration.
- The [Developing plug-ins](PluginDevelopment.md) subsection describes the first steps towards developing new plug-ins.

![Overview of the platform API](img/PlatformApi.png)
