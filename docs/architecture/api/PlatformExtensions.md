## Platform extensions

### Job {#job}
A job is a definition for a specific workflow in PLCverif. The most 
important built-in job is the VerificationJob (in the 
`cern.plcverif.verif.job` project). A job is in charge of invoking the 
services provided by the Platform to achieve a specific goal. 

PLCverif distinguishes two type of jobs.
- A _basic job_ is simply a workflow to be executed. It receives a specified output directory and the settings provided by the user, and based on that it can produce some artefacts. No further constraints are made for this type of job. Typically, basic jobs are for workflows which do not rely on the services of the PLCverif platform, however, it is beneficial to keep them in the same tool. Basic jobs implement the `IBasicJob` interface. They produce `BasicJobResult` outputs.

- A _CFA job_ is a workflow that requires the CFA representation of some PLC programs for the execution, provided by some parser. These CFAs can also be reduced. CFA jobs implement the `ICfaJob` interface (and as it inherits from the `IBasicJob`, they implement that one, too). They produce `JobResult` outputs.

#### Overview of the Job API
![Job API](img/JobApi.png)

#### Details of the Job interfaces
![Details of the Job interfaces](img/JobInterfaceDetails.png)

#### Defining a new job
To define a new job, the `IJobExtension` interface shall be implemented, and registered using the `cern.plcverif.base.extensions.job` extension point. 


#### Job result {#jobresult}

The job results (`BasicJobResult` and `JobResult`) contain lots of information at the end of the execution of the jobs. The various properties are expected to be filled by various objects or steps of the workflow.


##### Result of `IBasicJob` (`BasicJobResult`)
All properties of `BasicJobResult` are expected to be set by the `IBasicJob` implementations. However, the result classes extending `BasicJobResult` may have properties which shall be set by some of their extensions.

| Property | Type | Description | Can be null |
| -------- | ---- | ----------- | ----------- |
| `settings` | `Settings` | Settings used by the job. | yes |
| `outputDirectory` | `Path` | Directory for output files. | yes* |
| `programDirectory` | `Path` | Directory of PLCverif (used to resolve external tool locations) | no |

Notes:
- If the `outputDirectory` is not set, its value is `null`. However, if its value is accessed via the suggested `getOutputDirectoryOrCreateTemp()`, in case of `null` value a new temporary directory will be created and stored as `outputDirectory`. The return value of `getOutputDirectoryOrCreateTemp()` is never `null`.

###### Stages
Each job result define stages. A _stage_ is a step in the execution of job. Each stage is represented by a `JobStage` object that contains the name of the stage (`name`), optionally a summary (`summary`), optionally a set of tags (`tags` that are instances of `StageTag`), the start of its execution (`time`), its execution time (`lengthNs`) and its result/success (`status`). In addition, `JobStage` implements the `IPlcverifLogger`, thus log messages can be attached to stages. 

A stage is typically opened by `BasicJobResult.switchToStage(String stageName, StageTag... tags)` that switches to an already existing stage or creates (and returns) a new one with the given name. Upon the creation of a stage, the start time is automatically stored. If a new stage is opened and the previously open stage had an unknown status, its status will be changed to `Successful`, i.e., it is assumed that a stage was successful if not marked explicitly as unsuccessful. Furthermore, if a stage is closed, its duration will be automatically calculated. (Reopening a stage is also handled correctly.)



##### Result of `ICfaJob` (`JobResult`)
All properties of `JobResult` are expected to be set by the `ICfaJob` implementations. However, the result classes extending `JobResult` may have properties which shall be set by some of their extensions.

| Property | Type | Description | Can be null |
| -------- | ---- | ----------- | ----------- |
| `jobMetadata`| `JobMetadata` | Cfa job metadata. |  no |
| `inputFiles` | `List<String>` | List of input files used as input. | no |
| `parserResult` | `IParserLazyResult` | Parser memento that can be used to parse the source files and atoms later on. |  yes |
| `transformationGoal` | `CfaTransformationGoal` | CFA format that was achieved |  yes (if not set) |
| `inlined` | `boolean` | True if the CFA was inlined. |  no |
| `cfaDeclaration` | `CfaNetworkDeclaration` | CFA declaration corresponding to the source files and the requirement (potentially reduced). |  yes (if not set) |
| `cfaInstance` | `Optional<CfaNetworkInstance>` | CFA instance corresponding to the source files and the requirement (potentially reduced), if the model was instantitated |  no (empty if not set) |
| `variableTrace` | `IVariableTraceModel` | Variable trace between declaration and instance CFAs. |  no (empty if not set) |

- A `JobMetadata` stores essential metainformation about the CFA job: its unique identifier (`id`), name (`name`) and description (`description`) if given.
- An `IVariableTraceModel` is able to make translation between data references in the declaration CFA and variable references in the instance CFA.
- The `CfaTransformationGoal` defines the type of CFA that is to be achieved. It may be ``


### Parser / Language frontend {#parser}

A parser should be able to turn a certain type of input (specified by the parser itself, e.g. PLC code) into an AST (see `IAstReference`) and then a CFA declaration (see `CfaNetworkDeclaration`).


### CFA reduction {#reduction}

A reduction is a CFA transformation with the goal of reducing the size or complexity of the CFA model in some sense, preserving certain properties of it.
