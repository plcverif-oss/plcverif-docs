## Summary job

The goal of the summary job (`SummaryJob`) is to collect the given verification mementos, produced by the [summary verification reporter](../../library/reporters/README.md#summary), and to generate summary reports representing the verification results. The verification mementos are parsed into a collection of `VerificationMementoNode`s, which are then turned into various outputs by the _summary reporters_, implementing the `cern.plcverif.verif.summary.extensions.summaryreporter` extension points, essentially implementing the `ISummaryReporterExtension` and `ISummaryReporter` interfaces.

The summary job is a basic job, thus it implements only `IBasicJob`, but not `ICfaJob`.

### Verification memento metamodel {#metamodel}
The following verification memento metamodel shows the information that is available for the summary reporters.

![Verification memento metamodel](img/VerificationMementoMetamodel.png)


### Settings

The summary job can be configured using PLCverif settings (typically with the prefix `job.`) which will be represented by a `SummaryJobSettings` instance.

| Setting name (Type) | Description | Default value |
| ------------ | ----------- | ------------- |
| `summary_files` (`List<String>`) | Summary memento files to be compiled into a summary report.  They may contain wildcards (`*` or `?`). | --- |
| `reporters` (`List<String>`) | IDs of summary reporters to be used. | --- |


> **[info] More information**
>
> See the [library documentation](../../library/summaryreporters/README.md) to find more information about the built-in summary reporters.


