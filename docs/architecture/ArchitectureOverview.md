# Architecture

This chapter discusses the architecture of PLCverif.

**Platform and metamodels.** PLCverif is a platform for (PLC) program analysis, mainly for formal verification, but also for testing and static analysis. It provides:
 - A unified [workflow model](api/PlatformExtensions.md#job) for program analysis (`Job`),
    * A [verification-specific implementation](verif/VerificationJob.md) of this workflow model is also included (`VerificationJob`). This defines the concept of verification requirements, verification backends, verification reports.
 - [_Common metamodels_](metamodels/README.md) to describe analysis problems (Expression and CFA metamodels),
 - Platform-level support for:
    * [Language frontend](api/PlatformExtensions.md#parser) handling (which are responsible for parsing programs and representing them as CFAs),
	* [CFA reductions](api/PlatformExtensions.md#reduction),
	* Unified serialization and deserialization of [job configuration](api/Settings.md) (`Settings`).
	

**Core library.** Besides the platform definition, PLCverif contains a _library_ of core functionalities. This includes built-in jobs ([verification job](verif/VerificationJob.md)), and [verification backends](../library/backends/README.md), [CFA reductions](../library/reductions/README.md) and [verification reporters](../library/reporters/README.md) for the verification job.
The core libraries are discussed in the next chapter.
	
 
 
 
 
