## Expression metamodel

PLCverif uses a single expression metamodel for the CFA definitions (e.g. to describe guards or variable assignments) and for the temporal logic requirement descriptions. The expression metamodel is intended to be constructed programmatically, thus it requires that each expression node is typed, furthermore implicit type conversion is not permitted.


### Overview of types and literals {#types_and_literals}
![Types and literals defined in the expression metamodel](img/TypesLiterals.png)

**Types.** Every expression needs to be typed, i.e., to contain a type object (instance of `Type`). The expression metamodel defines four types: Boolean, integer, floating-point and string.
  - **Boolean** types are represented by `BoolType`. Temporal logic operations should have the `temporal` field set to true.
  - **Integer** types are represented by `IntType`.
    - The integer types are bounded. The `bits` field determines on how many bits will the value be stored. It shall be at least 1.
    - Integer types can be *signed* (`signed`=true) and unsigned (`signed`=true).
    - Unsigned integer types have a value range $$[0; 2^{'bits'}-1]$$. Signed integer types have a value range $$[-(2^{'bits'-1}); 2^{'bits'-1}-1]$$.
  - **Floating-point** types are represented by `FloatType`. They represent IEEE floating-point numbers with defined bit width.
    - The bit width of floating-point numbers is defined by `bits` that can be 16, 32 or 64 (`FloatWidth`).
  - **String types** are represented by `StringType`.
    - The maximum length in number of characters is defined by `maxLength`.

In addition, there is a representation of unknown types (`UnknownType`). Expressions containing unknown types cannot be evaluated.
    
    
**Type conversion.** The types of values and expressions can be altered using **type conversion** (`TypeConversion`). The `TypeConversion.type` defines the target type. Permitted conversions:

| from / to         | `BoolType` | `IntType` | `FloatType` | `StringType` |
| ----------------- | ---------- | --------- | ----------- | ------------ |
| **`BoolType`**    | OK         | false to 0, true to 1 | not possible | false to 'false', true to 'true' | 
| **`IntType`**     | not possible | OK (narrowing as in Java) | OK | OK |
| **`FloatType`**   | not possible | OK (truncated to integer) | OK (narrowing as in Java) | not defined |
| **`StringType`**  | not possible | not possible | not possible | OK (truncated if shorter) |

**Literals.** The metamodel defines literals for all four defined types (`BoolLiteral`, `IntLiteral`, `FloatLiteral`, `StringLiteral`). They all extend the abstract class `Literal`. Their `value` field stores their value.
There is no literal for the unknown type, however, the uninterpreted symbol (`UninterpretedSymbol`) is convenient for this purpose.



### Overview of basic expressions {#basic_expressions}
![Overview of the expression metamodel's core parts](img/BasicExpressions.png)

- Each atomic expression and operation implements the interface `Expression`. Consequently, each of them have a `getType()` method to get their type. Some of the expression nodes are explicitly typed (they implement the `ExplicitlyTyped` interface, the developer needs to take care of the assignment of the type), some others are implicitly typed.
- Atomic expressions (`AtomicExpression`) are expression nodes which do not have further children expression nodes.
    - Uninterpreted symbols (`UninterpretedSymbol`) can store a string which cannot be parsed momentarily. Expression trees with uninterpreted symbols typically cannot be evaluated.
    - Left values (`LeftValue`) are values which can be assigned, e.g., variables. The expression metamodel does not define any implementation of this abstract class, but the CFA metamodels have their variable representations integrated into the expression metamodel by inheriting from `LeftValue`.
    - The initial value (`InitialValue`) is a common superclass for values which can be used as initial values. The two descendants of this are the aforementioned `Literal` class and the `Nondeterministic` class.
    - The nondeterministic values (`Nondeterministic`) can be evaluated to a randomly chosen value from their type's value range.
- Unary expressions (`UnaryExpression`) are expression nodes which have exactly one argument or operand (`UnaryExpression.operand`). The following unary expression types are defined:
  - An **unary arithmetic expression** (`UnaryArithmeticExpression`) describes an unary arithmetic operation. Typical operators: negation, bitwise negation. The available operators (`UnaryArithmeticOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/UnaryArithmeticOperator.html).
  - An **unary logic expression** (`UnaryLogicExpression`) describes an unary logic operation. Typical operators: logic negation. The available operators (`UnaryLogicOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/UnaryLogicOperator.html).
  - An **unary CTL expression** (`UnaryCtlExpression`) describes an unary CTL operation. Typical operators: AX, AF, AG, EX, EF, EG. The available operators (`UnaryCtlOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/UnaryCtlOperator.html).
  - An **unary LTL expression** (`UnaryLtlExpression`) describes an unary LTL operation. Typical operators: X, F, G. The available operators (`UnaryLtlOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/UnaryLtlOperator.html).
  - The previously mentioned type conversion (`TypeCoversion`) is also an unary expression.
- Binary expressions (`BinaryExpression`) are expression nodes which have exactly two arguments or operands, a left operand (`BinaryExpression.leftOperand`) and a right operand (`BinaryExpression.rightOperand`). The following binary expression types are defined:
  - A **binary arithmetic expression** (`BinaryArithmeticExpression`) describes a binary arithmetic operation. Typical operators: +, -, *, /, modulo, power, bit shift, bit rotate. The available operators (`BinaryArithmeticOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/BinaryArithmeticOperator.html).
  - A **comparison expression** (`ComparisonExpression`) describes a binary logic comparison operation. Typical operators: $$=$$, $$\neq$$, $$\lt$$, $$\leq$$, $$\gt$$, $$\geq$$. The available operators (`ComparisonOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/ComparisonOperator.html).
  - A **binary logic expression** (`BinaryLogicExpression`) describes a binary logic operation. Typical operators: and, or, xor, implication. The available operators (`BinaryLogicOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/BinaryLogicOperator.html).
  - A **binary CTL expression** (`BinaryCtlExpression`) describes a binary CTL operation. Typical operators: AU, EU. The available operators (`BinaryCtlOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/BinaryCtlOperator.html).
  - A **binary LTL expression** (`BinaryLtlExpression`) describes a binary LTL operation. Typical operators: U. The available operators (`BinaryLtlOperator`) are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/BinaryLtlOperator.html).
- N-ary expressions (`NaryExpression`) are expression nodes which have one or more operands (`NaryExpression.operands`).
  - A **library function** (`LibraryFunction`) represents one of the pre-defined library functions (`LibraryFunctions`). Typical library functions: sin, cos, tan, sqrt, ln, log. The available functions are described in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/base/models/expr/LibraryFunctions.html).
  - An **uninterpreted function** (`UninterpretedFunction`) is a placeholder for an operation that cannot be parsed momentarily. It describes the operation to be performed using a string. Expression trees with uninterpreted functions typically cannot be evaluated.


> [info] **Naming convention for operations**
> 
> Notice the naming convention for the different operations in the expression metamodel. They are typically named using the following pattern: _{arity}{category}Expression_, their operator (`operator`) is a value from the enumeration _{arity}{category}Operator_. Notable exception is the `ComparisonExpression` where the name following the pattern would be unnecessarily redundant (~~BinaryComparisonExpression~~).

### Special expression nodes

- `BeginningOfCycle` is a predicate (thus has type `BoolType`) that is evaluated to true iff the currently active location is the initial location of the main automaton. Shall only be used in requirements.
- `EndOfCycle` is a predicate (thus has type `BoolType`) that is evaluated to true iff the currently active location is the end location of the main automaton. Shall only be used in requirements.

See more in the [detailed metamodel documentation](../../reference/ExprXcore.md).


### Factory {#factory}

The expression metamodel is implemented using EMF, and there is a generated factory available for it. However, we strongly suggest to use the `ExpressionSafeFactory` for the creation of the expression models. It provides a more convenient interface, and a plethora of checks for the validity of the given arguments.

There are two types of methods in this factory: 
- `createXxxxx()` methods are wrappers of the default EMF factory's `createXxxxx()` methods, but extended with checks. In addition, all mandatory data of the object to be created need to be provided as parameters to this factory method. In general, if an EMF object (`EObject`) is to be provided to these methods, it is the callers responability to create a copy of them if they are already contained by another EMF object. Exception will be thrown otherwise.
- Other methods provide short, easy-to use, "fluent" methods to create expression metamodels. They will make defensive copies of all EMF objects passed to them, no matter if they are contained or not.

An instance of the factory can be accessed using `ExpressionSafeFactory.INSTANCE`. Certain type checks may be very performance-intensive. These checks (but not all the others!) are disabled in the following instance: `ExpressionSafeFactory.INSTANCE_NON_STRICT`.

{% hint style='example' %}
**Example.** Assume that we already have two expressions `a` and `b`. Let us take as an example the creation of an expression _a AND b_. 

**Method 1 (highly discouraged): Use the built-in EMF factory**
```
BinaryLogicOperation r = ExprLangFactory.eINSTANCE.createBinaryLogicOperation();
r.setLeftOperand(a);  // or r.setLeftOperand(EcoreUtil.copy(a)); if already in use
r.setRightOperand(b);  // or r.setRightOperand(EcoreUtil.copy(b)); if already in use
r.setOperator(BinaryLogicOperator.AND);
```

**Method 2: Use the safe factory's create methods**
```
// if 'a' and 'b' are not in use:
BinaryLogicOperation r = 
  ExpressionSafeFactory.INSTANCE.createBinaryLogicOperation(
    a,
    b,
    BinaryLogicOperator.AND);

// or if 'a' and 'b' are already in use:
BinaryLogicOperation r = 
  ExpressionSafeFactory.INSTANCE.createBinaryLogicOperation(
    EcoreUtil.copy(a),
    EcoreUtil.copy(b),
    BinaryLogicOperator.AND);
```

**Method 3: Use the safe factory's fluent API**
```
BinaryLogicOperation r = ExpressionSafeFactory.INSTANCE.and(a, b);
// 'a' and 'b' will be copied anyway
```

Obviously, if the factory is used frequently, it makes sense to store it in a short-named local variable.
{% endhint %}



### Utilities {#utilities}

The `cern.plcverif.base.models.expr` package provides various additional utilities for expression handling.


- **Evaluation.** The `ExprEval` class can determine whether an expression can be evaluated to a constant (`isComputableConstant()`). If so, the `literalRepresentation(Expression)` method can return the equivalent literal representation, or the  `toBoolean()`, `toLong()`,`toDouble()` methods can return the result using Java primitive types. The `compute(Expression)` method replaces any computable subtree of the given expression tree with the corresponding literals. The returned node is the new root node. These methods can be accessed via static methods in the `ExprUtils` too which is the preferred way to use them.
- **Type cache.** Determining the type of the nodes of a big expression tree can be expensive, as the type of certain nodes is computed on the fly. The `ExprTypeCache` provides a cache for types accessed via its `getType()` method.
- **Normal forms.** The `NormalFormUtils` utility class provides methods (`toConjunctiveNf()`) to transform an expression tree into CNF if possible. It also provides certain expression simplifications.
- **String representation.** The `ExprToString` class provides textual representation of expressions for diagnostic purposes. If necessary, `AbstractExprToString` can be subclassed to create custom textual representations for expression node trees.
- **Validation.** 
Certain validations are defined in the `ExpressionValidation` class, however, it does not check the constraints prescribed by the metamodel itself. It is advised to use `CfaValidation.validate()` instead for the complete CFA.
- **Type utilities.** The `TypeUtils` utility class provides static convenience methods to check the types of expression nodes (e.g., `isFloatType()`, `hasFloatType()`). In addition, its only instance, `TypeUtils.INSTANCE` provides `dataTypeEquals(Type type1, Type type2)` to compare the equality of two types.
- **Expression utilities.** The `ExprUtils` utility class provides various utilities, such as `isTrueLiteral()`, `createLiteralFromString()`, etc.
- **Temporal logic expression utilities.** The `TlExprUtils` utility class provides various utilities for CTL and LTL expressions, such as methods to determine if an expression is pure CTL or pure LTL.  
  
In addition, the `cern.plcverif.base.models.exprparser` package contains an expression parser ([`ExpressionParser`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/exprparser/ExpressionParser.html)) that can be used to parse textual inputs matching the defined [expression grammar](../../reference/ExpressionGrammar.md).