# Metamodels

PLCverif provides metamodels for code analysis. These metamodels provide essential resources for the PLCverif plug-ins and makes sure the interconnectibility between the different modules of PLCverif. All these metamodels are implemented in the `cern.plcverif.base.models` plug-in.

The foundation of the metamodel hierarchy is a typed [**expression metamodel**](ExpressionMetamodel.md). It defines atomic expressions and operations (arithmetic, logic, comparison, temporal logic). There is also a parser provided for this metamodel (see [`ExpressionParser`]({{ book.api_docs_core_url }}/cern/plcverif/base/common/settings/help/SettingsHelp.html)). 

The control flow automata (CFA) metamodels, which are used for the representation of the PLC programs to be analyzed, build on the expression models too. The guards and assignments use the expressions from the expression metamodel, and the type system is the same too. 

PLCverif supports two types of control flow automata models: CFA declaration models and CFA instance models.
In a **CFA declaration**, the modeling is on the level of declarations. The defined automata are only structural definitions. To execute an automaton, a context (representing the automaton instance) shall be given that is a field with a data type corresponding to the automaton.
In a **CFA instance**, the modeling is on the level of automata instances. Only global variables exists, and there are no contexts for automata execution.
The common parts of these two CFA metamodels are defined in the **CFA base metamodel**.

![Metamodels overview](img/metamodels-overview.png)

- [Expressions](ExpressionMetamodel.md)
  * [Basic expressions](ExpressionMetamodel.md#basic_expressions)
  * [Types and Literals](ExpressionMetamodel.md#types_and_literals)
  * [Detailed metamodel description (reference)](../../reference/ExprXcore.md)
  * [Expression parser grammar (reference)](../../reference/ExpressionGrammar.md)
- CFA
  * [CFA base](CfaBase.md) ([detailed metamodel description](../../reference/CfaBaseXcore.md))
  * [CFA declaration](CfaDeclaration.md) ([detailed metamodel description](../../reference/CfaDeclarationXcore.md))
  * [CFA instance](CfaInstance.md) ([detailed metamodel description](../../reference/CfaInstanceXcore.md))