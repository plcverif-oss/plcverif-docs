## CFA Base

The _CFA Base_ defines the generic concepts which are used in both the [CFA Declaration](CfaDeclaration.md) and [CFA Instance](CfaInstance.md) metamodels, such as locations and transitions.

![CFA base metamodel overview](img/CfaBase.png)

### Core features
- The root of the hierarchy is the abstract superclass `CfaNetworkBase`, representing a **CFA network**.
- The CFA network's concrete descendants contain one or more **automata**. One of them is the so-called _main automaton_. The automata are subclasses of `AutomatonBase`.
- Every automaton (`AutomatonBase`) contain *locations* and *transitions*. In each automaton, one of the contained location is the _initial location_, and one of them is the _end location_. 
- Every **location** has zero or more incoming transitions (`incoming`) and zero or more outgoing transitions (`outgoing`). The outgoing transitions should be mutually exclusive, i.e., there should not be an overlap between their guards.
- The **transitions** are descendants of the abstract `Transition` class. Each transition has a _source_ (`source`) and a _target_ (`target`) location. They also have a _guard_ attached to it (`condition`). A transition can fire only if the guard is evaluated to true.

### Additional features
- Certain objects (descendants of `Freezable`) can be **frozen** which indicates that they should not be removed from the model during the transformations.
- The automata, locations and transitions can be annotated. Each annotation is inherited from the `Annotation` abstract class. In order to have typed parent references and to avoid incompatible types, `AutomatonAnnotation`, `LocationAnnotation`, `TransitionAnnotation` abstract annotations are defined as well. Most of the concrete descendants are in other metamodels.
- This metamodel defines in addition the **else expression** (`ElseExpression`). It can only be used in the guard of a transition and it cannot be part of an expression subtree. The else expression on transition $$t$$ is evaluated to true if and only if for every $$u \in t.'source'.'outgoing'$$ where $$u \neq t$$ the $$u.'condition'$$ is evaluated to false. For a location $$l$$ at most one of the transitions $$l.'outgoing'$$ may have else expression as guard.

See more in the [detailed metamodel documentation](../../reference/CfaBaseXcore.md).