##  Expr.xcore metamodel description

###  Table of contents
- [AtomicExpression](#anchor13)
- [BeginningOfCycle](#anchor45)
- [BinaryArithmeticExpression](#anchor25)
- [BinaryArithmeticOperator](#anchor23)
- [BinaryCtlExpression](#anchor35)
- [BinaryCtlOperator](#anchor33)
- [BinaryExpression](#anchor15)
- [BinaryLogicExpression](#anchor31)
- [BinaryLogicOperator](#anchor29)
- [BinaryLtlExpression](#anchor39)
- [BinaryLtlOperator](#anchor37)
- [BoolLiteral](#anchor48)
- [BoolType](#anchor8)
- [ComparisonExpression](#anchor27)
- [ComparisonOperator](#anchor26)
- [ElementaryType](#anchor3)
- [EndOfCycle](#anchor44)
- [ExplicitlyTyped](#anchor11)
- [Expression](#anchor12)
- [FloatLiteral](#anchor47)
- [FloatType](#anchor6)
- [FloatWidth](#anchor5)
- [InitialValue](#anchor18)
- [IntLiteral](#anchor46)
- [IntType](#anchor4)
- [LeftValue](#anchor17)
- [LibraryFunction](#anchor43)
- [LibraryFunctions](#anchor42)
- [Literal](#anchor19)
- [NaryExpression](#anchor16)
- [Nondeterministic](#anchor21)
- [StringLiteral](#anchor49)
- [StringType](#anchor9)
- [TemporalBoolType](#anchor7)
- [Type](#anchor1)
- [TypeConversion](#anchor40)
- [Typed](#anchor10)
- [UnaryArithmeticExpression](#anchor24)
- [UnaryArithmeticOperator](#anchor22)
- [UnaryCtlExpression](#anchor34)
- [UnaryCtlOperator](#anchor32)
- [UnaryExpression](#anchor14)
- [UnaryLogicExpression](#anchor30)
- [UnaryLogicOperator](#anchor28)
- [UnaryLtlExpression](#anchor38)
- [UnaryLtlOperator](#anchor36)
- [UninterpretedFunction](#anchor41)
- [UninterpretedSymbol](#anchor20)
- [UnknownType](#anchor2)

###  Package `cern.plcverif.base.models.expr`

####  Abstract class `Type` {#anchor1}

_Represents data type that are contained by ExplicitlyTyped model elements and_
_	returned by Typed model elements. Instances of this class and any subclasses must be compared by _
_	value instead of by reference, as every ExplicitlyTyped instance will have its own copy._

**Extends**: `EObject`



**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _Returns true if the current type is considered equal to the other type._

```
@GenModel(documentation="Represents data type that are contained by ExplicitlyTyped model elements and
	returned by Typed model elements. Instances of this class and any subclasses must be compared by 
	value instead of by reference, as every ExplicitlyTyped instance will have its own copy.")
abstract class Type {
		@GenModel(documentation="Returns true if the current type is considered equal to the other type.")
	op boolean dataTypeEquals(Type other) { throw new UnsupportedOperationException("Must be overridden by the subclasses."); }
}
```
####  Class `UnknownType` {#anchor2}

_Represents a data type that is currently unknown. Instances of this class are _
_	used with UninterpretedSymbols._

**Extends**: [Type](#anchor1)



**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _An unknown type is never equal to anything._

```
@GenModel(documentation="Represents a data type that is currently unknown. Instances of this class are 
	used with UninterpretedSymbols.")
class UnknownType extends Type {
	@GenModel(documentation="An unknown type is never equal to anything.")
	op boolean dataTypeEquals(Type other) {
		return false;
	}
}
```
####  Abstract class `ElementaryType` {#anchor3}

_The base class for all elementary (non-complex) types._

**Extends**: [Type](#anchor1)




```
@GenModel(documentation="The base class for all elementary (non-complex) types.")
abstract class ElementaryType extends Type {
}
```
####  Class `IntType` {#anchor4}

_Represents an integer type that can be signed or unsigned and has a bit width that _
_	includes the potential representation of the sign._

**Extends**: [ElementaryType](#anchor3)

**Attributes**:
- **signed** : `EBoolean`
    * Default value: true
- **bits** : `EInt`
    * Default value: 16


**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _Returns true if the other type is an IntType with the same sign and bit width._
- **getUnsignedBits**() : `EInt`
    * _Returns the number of bits that can be used to represent nonnegative numbers_

```
@GenModel(documentation="Represents an integer type that can be signed or unsigned and has a bit width that 
	includes the potential representation of the sign.")
class IntType extends ElementaryType {
	boolean signed = "true"
	int bits = "16"
		@GenModel(documentation="Returns true if the other type is an IntType with the same sign and bit width.")
	op boolean dataTypeEquals(Type other) {
		if (other instanceof IntType) {
			return (other.signed == this.signed && other.bits == this.bits);
		}
		return false;
	}
		@GenModel(documentation="Returns the number of bits that can be used to represent nonnegative numbers")
	op int getUnsignedBits() {
		if (signed) {
			return bits - 1
		} else {
			return bits
		}
	}
}
```
####  Class `FloatType` {#anchor6}

_Represents a float type that has a bit width specified by a FloatWidth._

**Extends**: [ElementaryType](#anchor3)

**Attributes**:
- **bits** : [FloatWidth](#anchor5)
    * Default value: 16


**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _Returns true if the other type is an FloatType with the same bit width._

```
@GenModel(documentation="Represents a float type that has a bit width specified by a FloatWidth.")
class FloatType extends ElementaryType {
	FloatWidth bits = "16"
	@GenModel(documentation="Returns true if the other type is an FloatType with the same bit width.")
	op boolean dataTypeEquals(Type other) {
		if (other instanceof FloatType) {
			return (other.bits == this.bits);
		}
		return false;
	}
}
```
####  Class `TemporalBoolType` {#anchor7}

_Represents a temporal Boolean type. A temporal Boolean_
_	cannot be evaluated without a model providing a behavior to check against._

**Extends**: [ElementaryType](#anchor3)



**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _Returns true if the other type is a TemporalBoolType._

```
@GenModel(documentation="Represents a temporal Boolean type. A temporal Boolean
	cannot be evaluated without a model providing a behavior to check against.")
class TemporalBoolType extends ElementaryType {
	@GenModel(documentation="Returns true if the other type is a TemporalBoolType.")
	op boolean dataTypeEquals(Type other) {
		return other instanceof TemporalBoolType;
	}
}
```
####  Class `BoolType` {#anchor8}

_Represents a Boolean type._

**Extends**: [TemporalBoolType](#anchor7)



**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _Returns true if the other type is an BoolType._

```
@GenModel(documentation="Represents a Boolean type.")
class BoolType extends TemporalBoolType {
	@GenModel(documentation="Returns true if the other type is an BoolType.")
	op boolean dataTypeEquals(Type other) {
		return other instanceof BoolType;
	}
}
```
####  Class `StringType` {#anchor9}

_Represents a string type that has a maximum length._

**Extends**: [ElementaryType](#anchor3)

**Attributes**:
- **maxLength** : `EInt`
    * _Maximum lenght of the string in characters._
    * Default value: 255


**Operations**:
- **dataTypeEquals**(other :  [Type](#anchor1)) : `EBoolean`
    * _Returns true if the other type is an StringType with the same maximum length._

```
@GenModel(documentation="Represents a string type that has a maximum length.")
class StringType extends ElementaryType {
	@GenModel(documentation="Maximum lenght of the string in characters.")
	int maxLength = "255"
	@GenModel(documentation="Returns true if the other type is an StringType with the same maximum length.")
	op boolean dataTypeEquals(Type other) {
		if (other instanceof StringType) {
			return (other.maxLength == this.maxLength);
		}
		return false;
	}
}
```
####  Interface `Typed` {#anchor10}

_Represents a model element that has a type (either contained or derived)._

**Extends**: `EObject`



**Operations**:
- **getType**() : [Type](#anchor1)
    * _Returns the type of the implementing model element. The returned type is _
    _		always contained, but not necessarily in the implementing element._

```
@GenModel(documentation="Represents a model element that has a type (either contained or derived).")
interface Typed {
		@GenModel(documentation="Returns the type of the implementing model element. The returned type is 
		always contained, but not necessarily in the implementing element.")
	op Type getType()  }
```
####  Interface `ExplicitlyTyped` {#anchor11}

_Represents a model element whose type is explicitly specified._

**Extends**: [Typed](#anchor10)


**References**:
- **type**  [1..1]: [Type](#anchor1)
    * _Stores the type of the implemented model element. The getter method overrides_
      _		method getType() in the Typed interface._
    * Containment: contains


```
@GenModel(documentation="Represents a model element whose type is explicitly specified.")
interface ExplicitlyTyped extends Typed {
	@GenModel(documentation="Stores the type of the implemented model element. The getter method overrides
		method getType() in the Typed interface.")
	contains Type[1] ^type
}
```
####  Interface `Expression` {#anchor12}

_Represents an expression, that is, a node in an expression tree._

**Extends**: [Typed](#anchor10)




```
@GenModel(documentation="Represents an expression, that is, a node in an expression tree.")
interface Expression extends Typed {
}
```
####  Abstract class `AtomicExpression` {#anchor13}

_Generic representation for atomic expressions that will serve as the leaves of the expression tree._

**Extends**: [Expression](#anchor12)



**Operations**:
- **getType**() : [Type](#anchor1)

```
@GenModel(documentation="Generic representation for atomic expressions that will serve as the leaves of the expression tree.")
abstract class AtomicExpression extends Expression {
	op Type getType() {
		throw new UnsupportedOperationException();
	}	
}
```
####  Abstract class `UnaryExpression` {#anchor14}

_Generic representation for expressions of pattern 'f(A)' or '[op] A'_

**Extends**: [Expression](#anchor12)


**References**:
- **operand**  [1..1]: [Expression](#anchor12)
    * Containment: contains

**Operations**:
- **getType**() : [Type](#anchor1)
    * _By default, unary expressions have the same type as their _
    _		operands (to be overridden if this is not appropriate)._

```
@GenModel(documentation="Generic representation for expressions of pattern 'f(A)' or '[op] A'")
abstract class UnaryExpression extends Expression {
	contains Expression[1] operand
	@GenModel(documentation="By default, unary expressions have the same type as their 
		operands (to be overridden if this is not appropriate).")
	op Type getType() {
		return operand.^type
	}
}
```
####  Abstract class `BinaryExpression` {#anchor15}

_Generic representation for expressions of pattern 'A [op] B'._

**Extends**: [Expression](#anchor12)


**References**:
- **leftOperand**  [1..1]: [Expression](#anchor12)
    * Containment: contains
- **rightOperand**  [1..1]: [Expression](#anchor12)
    * Containment: contains

**Operations**:
- **getType**() : [Type](#anchor1)
    * _By default, binary expressions have to have operands of the same type_
    _		the same type, so the type returned by this method will be one of their types. If any of the _
    _		operands are of unknown type, the result is that UnknownType instance._

```
@GenModel(documentation="Generic representation for expressions of pattern 'A [op] B'.")
abstract class BinaryExpression extends Expression {
	contains Expression[1] leftOperand
	contains Expression[1] rightOperand
	@GenModel(documentation="By default, binary expressions have to have operands of the same type
		the same type, so the type returned by this method will be one of their types. If any of the 
		operands are of unknown type, the result is that UnknownType instance.")
	op Type getType() {
		val leftType = leftOperand.^type;
				if (leftType instanceof UnknownType) {
			return leftType;
		}
		val rightType = rightOperand.^type;
		if (rightType instanceof UnknownType) {
			return rightType;
		}
		if (leftType.dataTypeEquals(rightType) == false) {
			throw new IllegalStateException("Binary operation has operands of different types.")
		}
		if (leftType instanceof BoolType && !(rightType instanceof BoolType)) {
									return rightType;
		}
		return leftType;
	}
}
```
####  Abstract class `NaryExpression` {#anchor16}

_Generic representation for expressions of pattern 'f(A, ..., B)'._

**Extends**: [Expression](#anchor12)


**References**:
- **operands**  [1..*]: [Expression](#anchor12)
    * Containment: contains

**Operations**:
- **getType**() : [Type](#anchor1)

```
@GenModel(documentation="Generic representation for expressions of pattern 'f(A, ..., B)'.")
abstract class NaryExpression extends Expression {
	contains Expression[1..*] operands
	op Type getType() {
		throw new UnsupportedOperationException("Type is undefined for generic N-ary expressions.");
	}	
}
```
####  Abstract class `LeftValue` {#anchor17}

_generic representation for atomic expressions that represents pieces of _
_	data to which a value can be assigned._

**Extends**: [AtomicExpression](#anchor13)




```
@GenModel(documentation="generic representation for atomic expressions that represents pieces of 
	data to which a value can be assigned.")
abstract class LeftValue extends AtomicExpression {
}
```
####  Abstract class `InitialValue` {#anchor18}

_Generic representation for atomic expressions that can be initial values _
_	of a piece of data. Currently implemented by Literal and Nondeterministic._

**Extends**: [AtomicExpression](#anchor13)




```
@GenModel(documentation="Generic representation for atomic expressions that can be initial values 
	of a piece of data. Currently implemented by Literal and Nondeterministic.")
abstract class InitialValue extends AtomicExpression {
}
```
####  Abstract class `Literal` {#anchor19}

_Generic representation for atomic expressions that have a fixed value._

**Extends**: [InitialValue](#anchor18), [ExplicitlyTyped](#anchor11)




```
@GenModel(documentation="Generic representation for atomic expressions that have a fixed value.")
abstract class Literal extends InitialValue, ExplicitlyTyped {
}
```
####  Class `UninterpretedSymbol` {#anchor20}

_Representation of a uninterpreted symbol or placeholder. The type is usually_
_	an UnknownType (if assigned by a factory) but as an ExplicitlyTyped element, it can be set to more_
_	specific types._

**Extends**: [AtomicExpression](#anchor13), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **symbol**  [1..1]: `EString`



```
@GenModel(documentation="Representation of a uninterpreted symbol or placeholder. The type is usually
	an UnknownType (if assigned by a factory) but as an ExplicitlyTyped element, it can be set to more
	specific types.")
class UninterpretedSymbol extends AtomicExpression, ExplicitlyTyped {
	String [1] symbol
}
```
####  Class `Nondeterministic` {#anchor21}

_Representation a nondeterministic value of the specified type._

**Extends**: [InitialValue](#anchor18), [ExplicitlyTyped](#anchor11)




```
@GenModel(documentation="Representation a nondeterministic value of the specified type.")
class Nondeterministic extends InitialValue, ExplicitlyTyped {
}
```
####  Class `UnaryArithmeticExpression` {#anchor24}

_Representation for arithmetic expressions of pattern '[arithOp] A', _
_	where [arithOp] is MINUS or BITWISE_NOT._

**Extends**: [UnaryExpression](#anchor14)

**Attributes**:
- **operator** : [UnaryArithmeticOperator](#anchor22)



```
@GenModel(documentation="Representation for arithmetic expressions of pattern '[arithOp] A', 
	where [arithOp] is MINUS or BITWISE_NOT.")
class UnaryArithmeticExpression extends UnaryExpression {
	UnaryArithmeticOperator operator
}
```
####  Class `BinaryArithmeticExpression` {#anchor25}

_Representation for arithmetic expressions of pattern 'A [arithOp] B', _
_	where [arithOp] is PLUS, MINUS, MULTIPLICATION, DIVISION, MODULO, INTEGER_DIVISION, POWER,_
_	BITSHIFT, BITROTATE, BITWISE_OR, BITWISE_AND or BITWISE_XOR._

**Extends**: [BinaryExpression](#anchor15)

**Attributes**:
- **operator** : [BinaryArithmeticOperator](#anchor23)



```
@GenModel(documentation="Representation for arithmetic expressions of pattern 'A [arithOp] B', 
	where [arithOp] is PLUS, MINUS, MULTIPLICATION, DIVISION, MODULO, INTEGER_DIVISION, POWER,
	BITSHIFT, BITROTATE, BITWISE_OR, BITWISE_AND or BITWISE_XOR.")
class BinaryArithmeticExpression extends BinaryExpression {
	BinaryArithmeticOperator operator
}
```
####  Class `ComparisonExpression` {#anchor27}

_Representation for comparison expressions of pattern 'A [compOp] B',_
_	where [compOp] is EQUALS, NOT_EQUALS, LESS_THAN, GREATER_THAN, LESS_EQ or GREATER_EQ._

**Extends**: [BinaryExpression](#anchor15), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **operator** : [ComparisonOperator](#anchor26)



```
@GenModel(documentation="Representation for comparison expressions of pattern 'A [compOp] B',
	where [compOp] is EQUALS, NOT_EQUALS, LESS_THAN, GREATER_THAN, LESS_EQ or GREATER_EQ.")
class ComparisonExpression extends BinaryExpression, ExplicitlyTyped {
	ComparisonOperator operator
}
```
####  Class `UnaryLogicExpression` {#anchor30}

_Representation for logical expressions of pattern '[logicOp] A', _
_	where [logicOp] is NEG. Can only be used with [BoolType](#anchor8) arguments._

**Extends**: [UnaryExpression](#anchor14)

**Attributes**:
- **operator** : [UnaryLogicOperator](#anchor28)



```
@GenModel(documentation="Representation for logical expressions of pattern '[logicOp] A', 
	where [logicOp] is NEG. Can only be used with {@link BoolType} arguments.")
class UnaryLogicExpression extends UnaryExpression {
	UnaryLogicOperator operator
}
```
####  Class `BinaryLogicExpression` {#anchor31}

_Representation for logical expressions of pattern 'A [logicOp] B', _
_	where [logicOp] is AND, OR, XOR or IMPLIES. Can only be used with [BoolType](#anchor8) arguments._

**Extends**: [BinaryExpression](#anchor15)

**Attributes**:
- **operator** : [BinaryLogicOperator](#anchor29)


**Operations**:
- **getType**() : [Type](#anchor1)
    * _Binary logic expressions have to have operands of BoolType, but they can be_
    _		different with regard to the temporal attribute. If any of them is temporal, that type will be _
    _		returned. If any of the	operands are of unknown type, the result is that UnknownType instance._

```
@GenModel(documentation="Representation for logical expressions of pattern 'A [logicOp] B', 
	where [logicOp] is AND, OR, XOR or IMPLIES. Can only be used with {@link BoolType} arguments.")
class BinaryLogicExpression extends BinaryExpression {
	BinaryLogicOperator operator
	@GenModel(documentation="Binary logic expressions have to have operands of BoolType, but they can be
		different with regard to the temporal attribute. If any of them is temporal, that type will be 
		returned. If any of the	operands are of unknown type, the result is that UnknownType instance.")
	op Type getType() {
				val leftType = leftOperand.^type
		if (leftType instanceof UnknownType) {
			return leftType;
		}
		val rightType = rightOperand.^type
		if (rightType instanceof UnknownType) {
			return rightType
		}
		if (leftType instanceof BoolType && rightType instanceof BoolType) {
			return leftType;
		} else if ((leftType instanceof TemporalBoolType && rightType instanceof TemporalBoolType)) {
			return if (rightType instanceof BoolType) leftType else rightType; 
		} else 
		throw new IllegalStateException("Invalid types for operands of binary boolean expression.")
	}
}
```
####  Class `UnaryCtlExpression` {#anchor34}

_Representation for CTL expressions of pattern '[ctlOp] A',_
_	where [ctlOp] is AX, AF, AG, EX, EF or EG._

**Extends**: [UnaryExpression](#anchor14), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **operator** : [UnaryCtlOperator](#anchor32)



```
@GenModel(documentation="Representation for CTL expressions of pattern '[ctlOp] A',
	where [ctlOp] is AX, AF, AG, EX, EF or EG.")
class UnaryCtlExpression extends UnaryExpression, ExplicitlyTyped {
	UnaryCtlOperator operator
}
```
####  Class `BinaryCtlExpression` {#anchor35}

_Representation for CTL expressions of pattern 'A [ctlOp] B',_
_	where [ctlOp] is AU or EU._

**Extends**: [BinaryExpression](#anchor15), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **operator** : [BinaryCtlOperator](#anchor33)



```
@GenModel(documentation="Representation for CTL expressions of pattern 'A [ctlOp] B',
	where [ctlOp] is AU or EU.")
class BinaryCtlExpression extends BinaryExpression, ExplicitlyTyped {
	BinaryCtlOperator operator
}
```
####  Class `UnaryLtlExpression` {#anchor38}

_Representation for LTL expressions of pattern '[ltlOp] A', _
_	where [ltlOp] is X, F, G._

**Extends**: [UnaryExpression](#anchor14), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **operator** : [UnaryLtlOperator](#anchor36)



```
@GenModel(documentation="Representation for LTL expressions of pattern '[ltlOp] A', 
	where [ltlOp] is X, F, G.")
class UnaryLtlExpression extends UnaryExpression, ExplicitlyTyped {
	UnaryLtlOperator operator
}
```
####  Class `BinaryLtlExpression` {#anchor39}

_Representation for LTL expressions of pattern 'A [ltlOp] B', _
_	where [ltlOp] is U or R._

**Extends**: [BinaryExpression](#anchor15), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **operator** : [BinaryLtlOperator](#anchor37)



```
@GenModel(documentation="Representation for LTL expressions of pattern 'A [ltlOp] B', 
	where [ltlOp] is U or R.")
class BinaryLtlExpression extends BinaryExpression, ExplicitlyTyped {
	BinaryLtlOperator operator
}
```
####  Class `TypeConversion` {#anchor40}

_Representation for explicit type cast expressions. The type specified though_
_	ExplicitlyTyped is the target type._

**Extends**: [UnaryExpression](#anchor14), [ExplicitlyTyped](#anchor11)




```
@GenModel(documentation="Representation for explicit type cast expressions. The type specified though
	ExplicitlyTyped is the target type.")
class TypeConversion extends UnaryExpression, ExplicitlyTyped {
}
```
####  Class `UninterpretedFunction` {#anchor41}

_Representation for uninterpreted n-ary functions._

**Extends**: [NaryExpression](#anchor16), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **symbol**  [1..1]: `EString`



```
@GenModel(documentation="Representation for uninterpreted n-ary functions.")
class UninterpretedFunction extends NaryExpression, ExplicitlyTyped {
	String [1] symbol
}
```
####  Class `LibraryFunction` {#anchor43}

_Representation for interpreted n-ary library functions._

**Extends**: [NaryExpression](#anchor16), [ExplicitlyTyped](#anchor11)

**Attributes**:
- **function** : [LibraryFunctions](#anchor42)



```
@GenModel(documentation="Representation for interpreted n-ary library functions.")
class LibraryFunction extends NaryExpression, ExplicitlyTyped {
	LibraryFunctions function
}
```
####  Class `EndOfCycle` {#anchor44}

_Predicate that is true if and only if the currently active location represents the end of the cyclic execution._

**Extends**: [Expression](#anchor12), [ExplicitlyTyped](#anchor11)




```
@GenModel(documentation="Predicate that is true if and only if the currently active location represents the end of the cyclic execution.")
class EndOfCycle extends Expression, ExplicitlyTyped { }
```
####  Class `BeginningOfCycle` {#anchor45}

_Predicate that is true if and only if the currently active location represents the beginning of the cyclic execution._

**Extends**: [Expression](#anchor12), [ExplicitlyTyped](#anchor11)




```
@GenModel(documentation="Predicate that is true if and only if the currently active location represents the beginning of the cyclic execution.")
class BeginningOfCycle extends Expression, ExplicitlyTyped { 
}
```
####  Class `IntLiteral` {#anchor46}

_Representation for an integer literal value._

**Extends**: [Literal](#anchor19)

**Attributes**:
- **value** : `ELong`
    * Modifiers: Unsettable



```
@GenModel(documentation="Representation for an integer literal value.")
class IntLiteral extends Literal {
	unsettable long value
}
```
####  Class `FloatLiteral` {#anchor47}

_Representation for a float literal value._

**Extends**: [Literal](#anchor19)

**Attributes**:
- **value** : `EDouble`
    * Modifiers: Unsettable



```
@GenModel(documentation="Representation for a float literal value.")
class FloatLiteral extends Literal {
	unsettable double value
}
```
####  Class `BoolLiteral` {#anchor48}

_Representation for a boolean literal value._

**Extends**: [Literal](#anchor19)

**Attributes**:
- **value** : `EBoolean`
    * Modifiers: Unsettable



```
@GenModel(documentation="Representation for a boolean literal value.")
class BoolLiteral extends Literal {
	unsettable boolean value
}
```
####  Class `StringLiteral` {#anchor49}

_Representation for a string literal value._

**Extends**: [Literal](#anchor19)

**Attributes**:
- **value** : `EString`
    * Modifiers: Unsettable



```
@GenModel(documentation="Representation for a string literal value.")
class StringLiteral extends Literal {
	unsettable String value
}
```

####  Enum `FloatWidth` {#anchor5}

_Represents the allowed float widths. Literal and value is set accordingly._

**Literals**:
- `B16` (16) = 16
- `B32` (32) = 32
- `B64` (64) = 64

```
@GenModel(documentation="Represents the allowed float widths. Literal and value is set accordingly.")
enum FloatWidth {
	B16 as "16" = 16,
	B32 as "32" = 32,
	B64 as "64" = 64
}
```
####  Enum `UnaryArithmeticOperator` {#anchor22}

_Representation of supported unary arithmetic operators._

**Literals**:
- `MINUS` 
- `BITWISE_NOT` 

```
@GenModel(documentation="Representation of supported unary arithmetic operators.")
enum UnaryArithmeticOperator {
	MINUS, BITWISE_NOT
}
```
####  Enum `BinaryArithmeticOperator` {#anchor23}

_Representation of supported binary arithmetic operators. They can only be used on [IntType](#anchor4) and [FloatType](#anchor6) arguments. Additional restrictions apply._

**Literals**:
- `PLUS` : _Addition operator._
- `MINUS` : _Subtraction operator._
- `MULTIPLICATION` : _Multiplication operator._
- `DIVISION` : _Division operator. Can only be used on [FloatType](#anchor6) arguments._
- `MODULO` : _Remainder operator. Produces the remainder after the quotient of its operands rounded toward 0. The result value is such that `(a INTEGER_DIVISION b) MULTIPLICATION b + (a MODULO b)` The result is negative iff the left argument is negative. This semantics coincides with the Java semantics of remainder (`%`) operator._
- `INTEGER_DIVISION` : _Integer division operator. Produces the quotient of its operands, rounded toward 0. The result is negative iff the divident is negative. Examples: `5 MOD 3 = 5 MOD (-3) = 2`, `(-5) MOD 3 = (-5) MOD (-3) = -2`. This semantics coincides with the Java semantics of division (`/`) operator._
- `POWER` : _Power operator. Produces the left operand to the power of the right operand._
- `BITSHIFT` : _Bit shift operator. Shifts the left operand's bits towards the least significant bit by the amount specified by the right operand, if the right operand is positive. 0s are shifted in. Can only be used on [IntType](#anchor4) arguments._
- `BITROTATE` : _Bit rotation operator. Shifts the left operand's bits towards the least significant bit by the amount specified by the right operand, if the right operand is positive. The values shifted out on one side are shifted in from the other side. Can only be used on [IntType](#anchor4) arguments._
- `BITWISE_OR` : _Bitwise OR operation. Can only be used on [IntType](#anchor4) arguments._
- `BITWISE_AND` : _Bitwise AND operation. Can only be used on [IntType](#anchor4) arguments._
- `BITWISE_XOR` : _Bitwise XOR operation. Can only be used on [IntType](#anchor4) arguments._

```
@GenModel(documentation="Representation of supported binary arithmetic operators. They can only be used on {@link IntType} and {@link FloatType} arguments. Additional restrictions apply.")
enum BinaryArithmeticOperator {
	@GenModel(documentation="Addition operator.")
	PLUS,
	@GenModel(documentation="Subtraction operator.")
	MINUS,
	@GenModel(documentation="Multiplication operator.")
	MULTIPLICATION,
	@GenModel(documentation="Division operator. Can only be used on {@link FloatType} arguments.")
	DIVISION,
	@GenModel(documentation="Remainder operator. Produces the remainder after the quotient of its operands rounded toward 0. The result value is such that {@code (a INTEGER_DIVISION b) MULTIPLICATION b + (a MODULO b)} The result is negative iff the left argument is negative. This semantics coincides with the Java semantics of remainder ({@code %}) operator.")
	MODULO,
	@GenModel(documentation="Integer division operator. Produces the quotient of its operands, rounded toward 0. The result is negative iff the divident is negative. Examples: {@code 5 MOD 3 = 5 MOD (-3) = 2}, {@code (-5) MOD 3 = (-5) MOD (-3) = -2}. This semantics coincides with the Java semantics of division ({@code /}) operator.")
	INTEGER_DIVISION,
	@GenModel(documentation="Power operator. Produces the left operand to the power of the right operand.")
	POWER,
	@GenModel(documentation="Bit shift operator. Shifts the left operand's bits towards the least significant bit by the amount specified by the right operand, if the right operand is positive. 0s are shifted in. Can only be used on {@link IntType} arguments.")
	BITSHIFT,		
	@GenModel(documentation="Bit rotation operator. Shifts the left operand's bits towards the least significant bit by the amount specified by the right operand, if the right operand is positive. The values shifted out on one side are shifted in from the other side. Can only be used on {@link IntType} arguments.")
	BITROTATE,		
	@GenModel(documentation="Bitwise OR operation. Can only be used on {@link IntType} arguments.")
	BITWISE_OR,
	@GenModel(documentation="Bitwise AND operation. Can only be used on {@link IntType} arguments.")
	BITWISE_AND,
	@GenModel(documentation="Bitwise XOR operation. Can only be used on {@link IntType} arguments.")
	BITWISE_XOR
}
```
####  Enum `ComparisonOperator` {#anchor26}

_Representation of supported (binary) comparison operators._

**Literals**:
- `EQUALS` 
- `NOT_EQUALS` 
- `LESS_THAN` 
- `GREATER_THAN` 
- `LESS_EQ` 
- `GREATER_EQ` 

```
@GenModel(documentation="Representation of supported (binary) comparison operators.")
enum ComparisonOperator {
	EQUALS,
	NOT_EQUALS,
	LESS_THAN,
	GREATER_THAN,
	LESS_EQ,
	GREATER_EQ
}
```
####  Enum `UnaryLogicOperator` {#anchor28}

_Representation of supported unary logic operators._

**Literals**:
- `NEG` 

```
@GenModel(documentation="Representation of supported unary logic operators.")
enum UnaryLogicOperator {
	NEG
}
```
####  Enum `BinaryLogicOperator` {#anchor29}

_Representation of supported binary logic operators. Can only be used on [BoolType](#anchor8) arguments._

**Literals**:
- `AND` 
- `OR` 
- `XOR` 
- `IMPLIES` 

```
@GenModel(documentation="Representation of supported binary logic operators. Can only be used on {@link BoolType} arguments.")
enum BinaryLogicOperator {
	AND,
	OR,
	XOR,
	IMPLIES }
```
####  Enum `UnaryCtlOperator` {#anchor32}

_Representation of supported unary CTL operators._

**Literals**:
- `AX` 
- `AF` 
- `AG` 
- `EX` 
- `EF` 
- `EG` 

```
@GenModel(documentation="Representation of supported unary CTL operators.")
enum UnaryCtlOperator {
	AX,
	AF,
	AG,
	EX,
	EF,
	EG
}
```
####  Enum `BinaryCtlOperator` {#anchor33}

_Representation of supported binary CTL operators._

**Literals**:
- `AU` 
- `EU` 

```
@GenModel(documentation="Representation of supported binary CTL operators.")
enum BinaryCtlOperator {
	AU,
	EU
}
```
####  Enum `UnaryLtlOperator` {#anchor36}

_Representation of supported unary LTL operators._

**Literals**:
- `X` 
- `F` 
- `G` 

```
@GenModel(documentation="Representation of supported unary LTL operators.")
enum UnaryLtlOperator {
	X,
	F,
	G
}
```
####  Enum `BinaryLtlOperator` {#anchor37}

_Representation of supported binary LTL operators._

**Literals**:
- `U` 
- `R` 

```
@GenModel(documentation="Representation of supported binary LTL operators.")
enum BinaryLtlOperator {
	U,
	R }
```
####  Enum `LibraryFunctions` {#anchor42}

_Representation of supported library functions._

**Literals**:
- `SIN` : _Sine of an angle specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's._
- `COS` : _Cosine of an angle specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's._
- `TAN` : _Tangent of an angle specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's._
- `ASIN` : _Arc sine. The result angle is specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's._
- `ACOS` : _Arc cosine. The result angle is specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's._
- `ATAN` : _Arc tangent. The result angle is specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's._
- `SQRT` : _Square root. Takes one FloatType operand. Its type is the same as its operand's._
- `EXP` : _Exponent of the given value (e^argument). Takes one FloatType operand. Its type is the same as its operand's._
- `LN` : _Natural logarithm. Takes one FloatType operand. Its type is the same as its operand's._
- `LOG` : _10-base logarithm. Takes one FloatType operand. Its type is the same as its operand's._

```
@GenModel(documentation="Representation of supported library functions.")
enum LibraryFunctions {
		@GenModel(documentation="Sine of an angle specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's.")
	SIN,
		@GenModel(documentation="Cosine of an angle specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's.")
	COS,
		@GenModel(documentation="Tangent of an angle specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's.")
	TAN,
		@GenModel(documentation="Arc sine. The result angle is specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's.")
	ASIN,
		@GenModel(documentation="Arc cosine. The result angle is specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's.")
	ACOS,
		@GenModel(documentation="Arc tangent. The result angle is specified as a radian measure. Takes one FloatType operand. Its type is the same as its operand's.")
	ATAN,
		@GenModel(documentation="Square root. Takes one FloatType operand. Its type is the same as its operand's.")
	SQRT,
		@GenModel(documentation="Exponent of the given value (e^argument). Takes one FloatType operand. Its type is the same as its operand's.")
	EXP,
		@GenModel(documentation="Natural logarithm. Takes one FloatType operand. Its type is the same as its operand's.")
	LN,
		@GenModel(documentation="10-base logarithm. Takes one FloatType operand. Its type is the same as its operand's.")
	LOG
}
```

