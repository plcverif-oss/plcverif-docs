# Overview figures

## Platform overview

![Platform API overview](../architecture/api/img/PlatformApi.png)

### Jobs
![Job API](../architecture/api/img/JobApi.png)
![Job API with details](../architecture/api/img/JobInterfaceDetails.png)

### Settings
![Settings metamodel](../architecture/api/img/SettingsMetamodel.png)

## Metamodels

### Expression
![Metamodel for basic expression](../architecture/metamodels/img/BasicExpressions.png)
![Metamodel for expression typing](../architecture/metamodels/img/TypesLiterals.png)

### CFA
![CFA base metamodel](../architecture/metamodels/img/CfaBase.png)
![CFA declaration metamodel](../architecture/metamodels/img/CfaDeclaration.png)
![CFA instance metamodel](../architecture/metamodels/img/CfaInstance.png)