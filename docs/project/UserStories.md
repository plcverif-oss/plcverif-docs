# User stories

> [info] **User stories**
> 
> User stories describe requirements towards the product to be developed typically in the following format:
> *As a [type of user], I want [some goal] so that [some reason].* 
> 
> They can be extended with eventual details and explications.


## Editor
* As a user, I want to import SCL/STL/symbol definition files so that I can analyse SCL/STL programs.
* As a user, I want to edit SCL/STL files so that I can create SCL/STL programs for analysis.

## Verification case
* As a user, I want to describe textually a verification case.
* As a user, I want to describe a verification case on a graphical interface.
* As a user, I want to select a PLC program to be verified.
* As a user, I want to select the entry block of the PLC program to be verified.
* As a user, I want to be able to verify a PLC program split into several files.
* As a user, I want to select and fill a requirement pattern so that I can define a requirement to be verified.
* As a user, I want to select a set of assertions to be verified so that I can limit the scope of the verification.
* As a power user, I want to define a requirement in CTL/LTL so that I can define requirements without restrictions.
* As a power user, I want to select and fine-tune a specific model checker algorithm so that I can influence the choice of the used algorithm. 

## Reductions
* As a user, I want to use automated model reductions so that the verification is feasible.
* As a power user, I want to fine-tune the reductions so that I can improve the verification performance.

## Reporting
* As a user, I want a textual feedback of the verification result so that I can assess the satisfaction of the defined requirement.
* As a user, I want a representation of the diagnostic trace (if exists) so that I can inspect the violation/satisfaction of the requirement.
* As a power user, I want to have a visualization of the generated model so that I can inspect the model.
