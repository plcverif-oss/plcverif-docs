# High-level development principles and decisions

* Maintainability
   - The source code of PLCverif shall have high quality.
   - The critical parts of PLCverif shall be covered by adequate tests.
   - The developer documentation of PLCverif shall cover the important high-level principles.
   - The code should be properly documented in code comments.
* Extensibility
   - PLCverif cannot support all possible use cases out of the box, therefore its architecture is designed to be highly extensible.
   - *PLC languages.* First, PLCverif will support Siemens SCL and STL. In future it can be extended to support SFC, Schneider programs, etc.
   - *Model checkers.* First, PLCverif will support NuSMV/nuXmv and Theta. In future it can be extended to support other FSM-based model checkers.
   - *Requirement specification.* First, PLCverif will support requirement patterns and assertions as requirement specification formalism. In future it can be extended to support other textual/tabular/graphical requirement specification methods.
  