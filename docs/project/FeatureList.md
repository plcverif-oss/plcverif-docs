# List of desired features

* **Parsing PLC programs** written in various languages
  * languages:
      - base Siemens (program layout, variable and block declarations, types)
      - Siemens SCL
      - Siemens STL
      - import symbol tables
  * extra features:
      - handling all source files of a project together (cross-file references)
  * features not to be supported:
      - pointers
      - symbol table editing
* **Representing PLC programs** in an intermediate formalism
  * intermediate formalism:
      - implements a CFA in a convenient way
      - allows reductions
      - allows several CFA's interconnection
      - no concurrency, limited modularization (no synchronization) 
      - allows (at least partial) reconstruction of the original code structure (annotations about control structures)
* Support for **requirement formalization**
  * various user-friendly representations  
      - requirement patterns
      - assertions in the code
      - in the future, tabular requirement description methods may be supported
  * representation in LTL and/or CTL in a *model checker-independent* way
* Support for **external model checkers**
  * support for general-purpose model checkers
      - NuSMV/nuXmv
      - Theta
  * support for software model checkers
      - CBMC
  * full integration, including input generation, execution and output parsing
* **Reporting**
  * human-readable report
  * contains counterexample if available
  * XML report for Jenkins integration