# GitLab project structure

The PLCverif development is supported by GitLab. The source files are stored in Git on the CERN GitLab instance, and the automatic builds and releases of PLCverif are performed by GitLab as well.


## Detailed project structure
The PLCverif sources are located under the https://gitlab.cern.ch/plcverif/ address. It is divided into several Git repositories, and the PLCverif plug-ins are further organized into subfolders. These are briefly overviewed below.

- [`cern.plcverif`](https://gitlab.cern.ch/plcverif/cern.plcverif) project: _Core part of the PLCverif implementation_
  * `bundles/`
    * `base/`: _Eclipse plug-ins implementing base framework features of PLCverif_
    * `verif/`: _Eclipse plug-ins implementing the verification job for PLCverif_
    * `library/`: _Library of plug-ins providing additional features to the framework (e.g., model checker wrappers, reporters, etc.)_
  * `bundles.gui/`: _Eclipse plug-ins related to the GUI features of PLCverif. It is separated from the non-GUI plug-ins to clearly separate the plug-ins with and without GUI dependencies._
  * `docs/`: _Documentation fragments that are generated based on the implementation, thus need to be kept together with the code_
  * `features/`: _Definitions for the features, i.e., collections of plug-ins that are typically used together_
  * `lib/`: _Wrappers of external (non-Eclipse) dependencies_
  * `releng/`: _Release engineering-related plug-ins, such as Eclipse update site (P2 repo) definition, API documentation generation, POM configuration files, target platform._
  * `tests/`: _Tests for the plug-ins defined in `bundles/`_
  * `tests.gui/`: _Tests for the plug-ins defined in `bundles.gui/`_
  * `xtext/`: _Xtext grammar-based plug-ins (they cannot easily follow the above structure, thus they are separated)_
- [`cern.plcverif.plc.step7`](https://gitlab.cern.ch/plcverif/cern.plcverif.plc.step7/) project: _PLC program parser for Siemens STEP 7 PLC programs_
- [`cern.plcverif.cli`](https://gitlab.cern.ch/plcverif/cern.plcverif.cli) project: _Command-line PLCverif frontend_
  * Internally, it follows a structure similar to the internal structure of the `cern.plcverif` project.
- [`cern.plcverif.gui`](https://gitlab.cern.ch/plcverif/cern.plcverif.gui) project: _Graphical PLCverif frontend_
  * Internally, it follows a structure similar to the internal structure of the `cern.plcverif` project.
- [`plcverif-docs`](https://gitlab.cern.ch/plcverif/plcverif-docs) project: _Developer and user documentation for PLCverif_


## Maven jobs and GitLab CI {#maven}

One of the advantages of using GitLab is to provide automated, reproducible build, release and deployment. For each code repository described above (`cern.plcverif`, `cern.plcverif.plc.step7`, `cern.plcverif.cli`, `cern.plcverif.gui`), the build and deployment process is done by Maven and Tycho, thus the process is described by POM files.

- Use `mvn clean verify` from the root of a project to make a fresh build.
  - The `--settings settings.xml` makes sure that the additional settings described in the `settings.xml` file is taken into account.
- Use `mvn install` from the root of a project to build and deploy the artefacts.
  - Due to some limitations of Tycho, `mvn install` may perform the deployment before all tests have been executed.
- The artifacts to be deployed by the `mvn install` command can be configured using _Maven profiles_. The profile to be activated can be selected using the `-P <profileName>` command line parameter. Multiple profiles can be activated at the same time, e.g., `-P <profile1>,<profile2>,<profile3>`.
  The following profiles are defined:
  * `uploadRepo`: uploads the P2 repository (Eclipse update site) to `http://cern.ch/plcverif-p2/` (not viewable from web browser). This repository is used for the dependency resolution between the different GitLab projects (e.g., the `cern.plcverif.plc.step7` project will fetch its dependencies from the `cern.plcverif` project from the above Eclipse update site.)
  * `uploadJavadoc`: uploads the Javadoc API documentation (as a website) to http://cern.ch/plcverif-p2/javadoc/
  * `uploadJacoco`: uploads the generated test coverage report to http://cern.ch/plcverif-p2/jacoco/
  * `uploadBinaries`: uploads the created release binaries (only for `cern.plcverif.cli` and `cern.plcverif.gui`) to http://cern.ch/plcverif-p2/release/
    * Note that the `uploadBinaries` is included in a separate stage for the GitLab CI jobs which need a single-click manual action. This is in order to avoid updating the release automatically.
  * All the above uploads will be performed using the credentials described in the `settings.xml` (referring to environment variables defined in GitLab).
- A typical build from command line looks like this:
  ```
  mvn clean verify --settings settings.xml
  mvn install -P uploadRepo,uploadJavadoc,uploadJacoco --settings settings.xml
  ```

> [info] PLCverif dependencies
> 
> PLCverif has some external dependencies, such as the model checker tools. In addition, PLCverif should be shipped with some default settings and demo project. Partially for technical, partially for licensing reasons, these dependencies are made available in separate ZIP files on the http://cern.ch/plcverif-p2/release/ webpage. These ZIP files are generated from `\\cern.ch\dfs\Projects\PLCverif\PLCverif3\include\`, using the `upload.bat` script located there (thus it is not done through GitLab).

## New release

When changes are pushed to the branch master of cern.plcverif, a pipeline that builds cern.plcverif, cern.plcverif.plc.step7, cern.plcverif.gui and cern.plcverif.cli is triggered. If the changes are pushed to cern.plcverif.plc.step7, only its following steps are triggered.

If no test fails and a new release wants to be deployed, we need to create a new tag following the naming vX.X.X in the projects cern.plcverif.gui and cern.plcverif.cli. Each of them triggers a pipeline to deploy the tool. They will wait for manual actions to be performed. Execute them and a new deployment will be done.

> [info] Docker (gitlab.cern)
> 
> In order to execute projects in gitlab, a Docker image is created with the project https://gitlab.cern.ch/plcverif-internal/continuous-verification/plcverif-cli-docker-java11/. When a new release is done, this project must be executed again to update the Docker image to the latest version of the Open Source PLCverif. By running this pipeline, it will execute the file install-plcverif.sh that takes the PLCverif version from https://gitlab.com/plcverif-oss/.

